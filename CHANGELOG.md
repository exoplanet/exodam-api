# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.16.0] - 07-03-2024

### Fix

- Now when we insert an exodict we make th update of ressources generate from the exodict.

## [0.15.0] - 28-02-2023

### Added

- Add `disk` in POST to add exodict.

## [0.14.0] - 16-02-2023

## Added

- Add "final exodict" in response of POST exodict.

## [0.13.0] - 15-02-2023

## Changed

- Change of isort config, now `Pytest imports` become `Test impotrs` and assertpy is
  in `Test imports`.

## [0.12.0] - 15-02-2023

### Changed

- Adapt with the new version of Exodam 0.56.0. Now `generate_and_insert_exodict` content
  of yaml file rather then paths.

## [0.11.0] - 02-02-2023

### Added

- Add `exodict` in POST to add exodict.

## [0.10.0] - 12-12-2023

### Added

- Add `add/news` to add news.
- Add `add` to get information on adding resources.

## [0.9.0] - 30-11-2023

### Added

- Add `editorial-convert-exodam` to convert editorial exodam YAML file

### Changed

- Rename `convert-exodam` in `scientific-convert-exodam`.

## [0.8.0] - 30-11-2023

### Changed

- Adapt the API with new version of exodam

## [0.7.0] - 05-10-2023

### Added

- Add convert-exodam API post command `/convert-exodam/`.

## [0.6.0] - 04-10-2023

### Changed

- Change response of `exodam/api/actions/diff/`. Now `diff` is the boolean response and
  `diff_str` is the result of the diff.

## [0.5.0] - 29-09-2023

### Added

- Add bootstrap and use it for the template `inde.html.j2`.

## [0.4.0] - 29-09-2023

### Added

- Add new path: `exodam/api/actions/` for the actions resources.
- Add an html page at path `/exodam/` with form to test the API.

### Changed

- Change path of check and diff command for:
  - `/exodam/api/actions/diff`.
  - `/exodam/api/actions/check`.
- Change path `/` for `/exodam/api/`
- Change base path of all command for `/exodam/api/<cmd>`.
- Change management of option in check API post command.

## [0.3.0] - 27-09-2023

### Added

- Add check API post command `/check/`.

## [0.2.0] - 21-09-2023

### Added

- Add diff API post command `/diff/`.

## [0.1.0] - 20-09-2023

### Added

- Add skel API get command `/skel/<SkelType>`.
- Add root `/` API get command.
