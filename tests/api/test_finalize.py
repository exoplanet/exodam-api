# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Exodam
from exodam_utils import load_json

# Third party imports
from fastapi import status

# Local imports
from ..conftest import TESTING_FILE_FOLDER_PATH
from .conftest import check_exp_msg_in_warning_msg

TESTING_FINALIZE_FOLDER_PATH = TESTING_FILE_FOLDER_PATH / "finalize"


@pytest.mark.parametrize(
    "action, file, exp_details, exp_convert_result, exp_warning, exp_exodam_result",
    [
        # SCIENTIFIC
        param(
            "scientific",
            "all_good.yaml",
            "All is ok",
            True,
            "Exodam syntax is good",
            load_json(TESTING_FINALIZE_FOLDER_PATH / "scientific/expected.exodam.json"),
            id="valid file scientific",
        ),
        param(
            "scientific",
            "missing_identity.yaml",
            "Something wrong",
            False,
            "Exodam struct and global information syntax isn't good",
            None,
            id="invalid_file -> check return False scientific",
        ),
        # EDITORIAL
        param(
            "editorial",
            "valid_file.yaml",
            "All is ok",
            True,
            "Exodam news syntax is good",
            load_json(TESTING_FINALIZE_FOLDER_PATH / "editorial/expected.exodam.json"),
            id="valid file editorial",
        ),
        param(
            "editorial",
            "none_valid.yaml",
            "Something wrong",
            False,
            "Exodam news syntax isn't good",
            None,
            id="invalid_file -> check return False editorial",
        ),
    ],
)
def test_api__convert_ok(
    testing_files_folder_path,
    client,
    action,
    file,
    exp_details,
    exp_convert_result,
    exp_warning,
    exp_exodam_result,
):
    with open(
        f"{testing_files_folder_path}/finalize/{action}/{file}",
        "rb",
    ) as open_file:
        post_file = {"file_to_convert": open_file}
        response = client.post(
            f"/exodam/api/actions/{action}-convert-exodam/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]["file"]).is_equal_to(file)
        assert_that(json_response["data"]["convert_exodam"]).is_equal_to(
            exp_convert_result
        )
        assert_that(
            check_exp_msg_in_warning_msg(
                exp_warning, json_response["data"]["warning_msg"]
            )
        ).is_equal_to(True)
        assert_that(json_response["data"]["exodam_file"]).is_equal_to(exp_exodam_result)


@pytest.mark.parametrize(
    "action, file, exp_warnings",
    [
        # SCIENTIFIC
        param(
            "scientific",
            "README.md",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. for file -> md scientific",
        ),
        param(
            "scientific",
            "invalid.yaml",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file invalid.yaml is empty.",
            ],
            id="file is empty scientific",
        ),
        # EDITORIAL
        param(
            "editorial",
            "README.md",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. for file -> md editorial",
        ),
        param(
            "editorial",
            "empty.yaml",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file empty.yaml is empty.",
            ],
            id="file is empty editorial",
        ),
    ],
)
def test_api__check_unsupported_media_type(
    testing_files_folder_path,
    client,
    action,
    file,
    exp_warnings,
):
    with open(
        f"{testing_files_folder_path}/finalize/{action}/{file}",
        "rb",
    ) as open_file:
        post_file = {"file_to_convert": open_file}
        response = client.post(
            f"/exodam/api/actions/{action}-convert-exodam/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
        assert_that(json_response["details"]).is_equal_to("Unsupported Media Type")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)
