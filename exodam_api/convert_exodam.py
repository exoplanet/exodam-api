"""Convert exodam function fro the API."""

# Standard imports
from pathlib import Path
from typing import Any, cast

# Exodam
from exodam import ExodictType, finalize_for_api

# Third party imports
from fastapi import UploadFile, status
from fastapi.responses import JSONResponse
from yaml import YAMLError

# Local imports
from .file_checker import FileChecker


def convert_exodam(
    file_to_convert: UploadFile,
    exodict_type: ExodictType,
) -> JSONResponse | dict[str, Any]:
    """
    Convert YAML exodam file into exodam.

    Args:
        file_to_convert: File to convert in exodam.
        exodict_type: Type of exodict use to convert.

    Args:
        API response.
    """
    file_path = Path(cast(str, file_to_convert.filename))
    try:
        content = FileChecker(file_to_convert).is_yaml().is_empty().content_or_raise()
    except (YAMLError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err.__str__()}",
                ],
            },
        )
    finally:
        file_to_convert.file.close()

    warning_msg: list[str] = []
    result = finalize_for_api(content, exodict_type, warning_msg)

    return {
        "details": "All is ok" if result is not None else "Something wrong",
        "data": {
            "file": file_path,
            "convert_exodam": bool(result is not None),
            "warning_msg": warning_msg,
            "exodam_file": result,
        },
    }
