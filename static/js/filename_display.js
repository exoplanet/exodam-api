// https://gist.github.com/zevwl/044164be9e5a530fbdbf113926ceeed9

// Helper function
const addFileNameToLabel = file => {
    const fileName = file.target.files[0].name
    const customLabel = file.target.nextElementSibling
    customLabel.textContent = fileName
}

// Aply to each `.custom-file-label`
document.querySelectorAll('.custom-file-input')
    .forEach(file => file.addEventListener('change', addFileNameToLabel))
