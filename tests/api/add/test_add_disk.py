# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Exodam
from exodam_utils import load_json

# Third party imports
from fastapi import status

# First party imports
from exodam_api import clean_none_data

# Local imports
from ...conftest import TESTING_FILE_FOLDER_PATH

TESTING_ADD_NEWS_FOLDER_PATH = TESTING_FILE_FOLDER_PATH / "add/disk"


@pytest.mark.parametrize(
    "file, exp_details, exp_inserted",
    [
        param(
            "one_disk.exodam.json",
            "All (1) disk(s) has been inserted. All good",
            load_json(TESTING_ADD_NEWS_FOLDER_PATH / "one_disk.exodam.json"),
            id="one disk",
        ),
        param(
            "multi_disk.exodam.json",
            "All (7) disk(s) has been inserted. All good",
            load_json(TESTING_ADD_NEWS_FOLDER_PATH / "multi_disk.exodam.json"),
            id="multi disk",
        ),
    ],
)
def test_api__add_news_ok(
    testing_files_folder_path,
    client,
    file,
    exp_details,
    exp_inserted,
):
    with open(
        f"{testing_files_folder_path}/add/disk/{file}",
        "rb",
    ) as open_file:
        post_file = {"disk_to_add": open_file}
        response = client.post(
            f"/exodam/api/disk/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]["file"]).is_equal_to(file)
        assert_that(json_response["data"]["inserted_data"]).is_equal_to(
            clean_none_data(exp_inserted["data"]["objects"])
        )


@pytest.mark.parametrize(
    "file, exp_warnings",
    [
        param(
            "README.md",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.exodam.json`, "
                "got: `.md`.",
            ],
            id="invalid ext. for file -> md",
        ),
        param(
            "invalid.exodam.json",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'json.decoder.JSONDecodeError'>",
                "Expecting value: line 1 column 1 (char 0)",
            ],
            id="file is empty",
        ),
    ],
)
def test_api__add_objects_unsupported_media_type(
    testing_files_folder_path,
    client,
    file,
    exp_warnings,
):
    with open(
        f"{testing_files_folder_path}/add/disk/{file}",
        "rb",
    ) as open_file:
        post_file = {"disk_to_add": open_file}
        response = client.post(
            f"/exodam/api/disk/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
        assert_that(json_response["details"]).is_equal_to("Unsupported Media Type")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)


@pytest.mark.parametrize(
    "file, exp_warnings",
    [
        param(
            "missing_exodict.exodam.json",
            [
                "Given file doesn't contains all mandatory information.",
                "We need an exodict, an exodict_version and data.",
                f"Missing: exodict",
            ],
            id="missing exodict",
        ),
        param(
            "missing_exodict_version.exodam.json",
            [
                "Given file doesn't contains all mandatory information.",
                "We need an exodict, an exodict_version and data.",
                f"Missing: exodict_version",
            ],
            id="missing exodict_version",
        ),
        param(
            "missing_data.exodam.json",
            [
                "Given file doesn't contains all mandatory information.",
                "We need an exodict, an exodict_version and data.",
                f"Missing: data, objects",
            ],
            id="missing data",
        ),
        param(
            "disk/missing_objects.exodam.json",
            [
                "Given file doesn't contains all mandatory information.",
                "We need an exodict, an exodict_version and data.",
                f"Missing: objects",
            ],
            id="missing objects",
        ),
        param(
            "disk/missing_exodict_and_objects.exodam.json",
            [
                "Given file doesn't contains all mandatory information.",
                "We need an exodict, an exodict_version and data.",
                f"Missing: exodict, objects",
            ],
            id="missing exodict and objects",
        ),
    ],
)
def test_api__add_objects_unprocessable_entity(
    testing_files_folder_path,
    client,
    file,
    exp_warnings,
):
    with open(
        f"{testing_files_folder_path}/add/{file}",
        "rb",
    ) as open_file:
        post_file = {"disk_to_add": open_file}
        response = client.post(
            f"/exodam/api/disk/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_422_UNPROCESSABLE_ENTITY
        )
        assert_that(json_response["details"]).is_equal_to("Unprocessable Entity")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)


def test_api__add_objects_py_linq_sql_error(
    testing_files_folder_path,
    client,
    remove_and_re_add_disk_table,
):
    with open(
        f"{testing_files_folder_path}/add/disk/one_disk.exodam.json",
        "rb",
    ) as open_file:
        post_file = {"disk_to_add": open_file}
        response = client.post(
            f"/exodam/api/disk/",
            files=post_file,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        assert_that(json_response["details"]).is_equal_to("Internal Server Error")
        assert_that("\n".join(json_response["warnings"])).contains(
            "psycopg.errors.UndefinedTable"
        )
