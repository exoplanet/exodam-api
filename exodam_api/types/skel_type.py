"""Skeleton Type fro the API."""

# Standard imports
from enum import EnumType, StrEnum


class SkelType(StrEnum):
    """Accepted file type for skeleton."""

    YAML = "yaml"
    YML = "yml"
    CSV = "csv"

    @classmethod
    def as_str_lst(cls: EnumType) -> list[str]:
        """Return a list of all values in str."""
        return [  # type: ignore[var-annotated]
            member.value for member in list(cls)  # type: ignore[attr-defined]
        ]
