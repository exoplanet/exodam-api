# Test imports
import pytest
from assertpy import assert_that
from pytest import param

# First party imports
from exodam_api import FileChecker as fC


def test_file_checker__class_yaml_file(FileCheckerInstanceYAML_file):
    assert FileCheckerInstanceYAML_file


def test_file_checker__class_yml_file(FileCheckerInstanceYML_file):
    assert FileCheckerInstanceYML_file


def test_file_checker__class_json_file(FileCheckerInstanceJSON_file):
    assert FileCheckerInstanceJSON_file


def test_file_checker__class_yaml_path(FileCheckerInstanceYAML_path):
    assert FileCheckerInstanceYAML_path


def test_file_checker__class_yml_path(FileCheckerInstanceYML_path):
    assert FileCheckerInstanceYML_path


def test_file_checker__class_json_path(FileCheckerInstanceJSON_path):
    assert FileCheckerInstanceJSON_path


def test_file_checker__class_other_type():
    file_checker = fC(1)
    assert file_checker._fail == True


@pytest.mark.parametrize(
    "funct",
    [
        param(fC.is_yaml, id="is yaml"),
        param(fC.is_empty, id="is empty"),
    ],
)
def test_file_checker__self_return_methods_yaml_file(
    FileCheckerInstanceYAML_file, funct
):
    assert_that(funct(FileCheckerInstanceYAML_file)).is_equal_to(
        FileCheckerInstanceYAML_file
    )


@pytest.mark.parametrize(
    "funct",
    [
        param(fC.is_yaml, id="is yaml"),
        param(fC.is_empty, id="is empty"),
    ],
)
def test_file_checker__self_return_methods_yaml_path(
    FileCheckerInstanceYAML_path, funct
):
    assert_that(funct(FileCheckerInstanceYAML_path)).is_equal_to(
        FileCheckerInstanceYAML_path
    )


@pytest.mark.parametrize(
    "funct",
    [
        param(fC.is_json, id="is json"),
        param(fC.is_empty, id="is empty"),
    ],
)
def test_file_checker__self_return_methods_json_file(
    FileCheckerInstanceJSON_file,
    funct,
):
    assert_that(funct(FileCheckerInstanceJSON_file)).is_equal_to(
        FileCheckerInstanceJSON_file
    )


@pytest.mark.parametrize(
    "funct",
    [
        param(fC.is_json, id="is json"),
        param(fC.is_empty, id="is empty"),
    ],
)
def test_file_checker__self_return_methods_json_path(
    FileCheckerInstanceJSON_path,
    funct,
):
    assert_that(funct(FileCheckerInstanceJSON_path)).is_equal_to(
        FileCheckerInstanceJSON_path
    )


def test_file_checker__content_or_raise_success_yaml_file(
    FileCheckerInstanceYAML_file,
    yaml_content_expected,
):
    assert_that(FileCheckerInstanceYAML_file.content_or_raise()).is_equal_to(
        yaml_content_expected
    )


def test_file_checker__content_or_raise_success_yaml_path(
    FileCheckerInstanceYAML_path,
    yaml_content_expected,
):
    assert_that(FileCheckerInstanceYAML_path.content_or_raise()).is_equal_to(
        yaml_content_expected
    )


def test_file_checker__content_or_raise_success_json_file(
    FileCheckerInstanceJSON_file,
    json_content_expected,
):
    assert_that(FileCheckerInstanceJSON_file.content_or_raise()).is_equal_to(
        json_content_expected
    )


def test_file_checker__content_or_raise_success_json_path(
    FileCheckerInstanceJSON_path,
    json_content_expected,
):
    assert_that(FileCheckerInstanceJSON_path.content_or_raise()).is_equal_to(
        json_content_expected
    )


def test_file_checker__content_or_raise_fail_file(FileCheckerInstanceYAML_file):
    FileCheckerInstanceYAML_file._fail = True
    with pytest.raises(ValueError):
        FileCheckerInstanceYAML_file.content_or_raise()


def test_file_checker__content_or_raise_fail_path(FileCheckerInstanceYAML_path):
    FileCheckerInstanceYAML_path._fail = True
    with pytest.raises(ValueError):
        FileCheckerInstanceYAML_path.content_or_raise()


def test_file_checker__pass_or_raise_success_yaml_file(FileCheckerInstanceYAML_file):
    assert_that(FileCheckerInstanceYAML_file.none_or_raise()).is_none()


def test_file_checker__pass_or_raise_success_yaml_path(FileCheckerInstanceYAML_path):
    assert_that(FileCheckerInstanceYAML_path.none_or_raise()).is_none()


def test_file_checker__pass_or_raise_success_json_file(FileCheckerInstanceJSON_file):
    assert_that(FileCheckerInstanceJSON_file.none_or_raise()).is_none()


def test_file_checker__pass_or_raise_success_json_path(FileCheckerInstanceJSON_path):
    assert_that(FileCheckerInstanceJSON_path.none_or_raise()).is_none()


def test_file_checker__pass_or_raise_fail_file(FileCheckerInstanceYAML_file):
    FileCheckerInstanceYAML_file._fail = True
    with pytest.raises(ValueError):
        FileCheckerInstanceYAML_file.none_or_raise()


def test_file_checker__pass_or_raise_fail_path(FileCheckerInstanceYAML_path):
    FileCheckerInstanceYAML_path._fail = True
    with pytest.raises(ValueError):
        FileCheckerInstanceYAML_path.none_or_raise()


@pytest.mark.parametrize(
    "base_msg, next_msg, res_expect",
    [
        param(None, "toto", "toto", id="without message before"),
        param("toto", "titi", "toto titi", id="with message before"),
    ],
)
def test_file_checker__concat_err_msg_file(
    FileCheckerInstanceYAML_file,
    base_msg,
    next_msg,
    res_expect,
):
    FileCheckerInstanceYAML_file._err_msg = base_msg
    FileCheckerInstanceYAML_file._concat_err_msg(next_msg)
    assert_that(FileCheckerInstanceYAML_file._err_msg).is_equal_to(res_expect)


@pytest.mark.parametrize(
    "base_msg, next_msg, res_expect",
    [
        param(None, "toto", "toto", id="without message before"),
        param("toto", "titi", "toto titi", id="with message before"),
    ],
)
def test_file_checker__concat_err_msg_path(
    FileCheckerInstanceYAML_path,
    base_msg,
    next_msg,
    res_expect,
):
    FileCheckerInstanceYAML_path._err_msg = base_msg
    FileCheckerInstanceYAML_path._concat_err_msg(next_msg)
    assert_that(FileCheckerInstanceYAML_path._err_msg).is_equal_to(res_expect)


def test_file_checker__load_content_yaml_success_file(
    FileCheckerInstanceYAML_file,
    yaml_content_expected,
):
    FileCheckerInstanceYAML_file._load_content()
    assert_that(FileCheckerInstanceYAML_file._content).is_equal_to(
        yaml_content_expected
    )


def test_file_checker__load_content_yaml_success_path(
    FileCheckerInstanceYAML_path,
    yaml_content_expected,
):
    FileCheckerInstanceYAML_path._load_content()
    assert_that(FileCheckerInstanceYAML_path._content).is_equal_to(
        yaml_content_expected
    )


def test_file_checker__load_content_yml_success_file(
    FileCheckerInstanceYML_file,
    yaml_content_expected,
):
    FileCheckerInstanceYML_file._load_content()
    assert_that(FileCheckerInstanceYML_file._content).is_equal_to(yaml_content_expected)


def test_file_checker__load_content_yml_success_path(
    FileCheckerInstanceYML_path,
    yaml_content_expected,
):
    FileCheckerInstanceYML_path._load_content()
    assert_that(FileCheckerInstanceYML_path._content).is_equal_to(yaml_content_expected)


def test_file_checker__load_content_json_success_file(
    FileCheckerInstanceJSON_file,
    json_content_expected,
):
    FileCheckerInstanceJSON_file._load_content()
    assert_that(FileCheckerInstanceJSON_file._content).is_equal_to(
        json_content_expected
    )


def test_file_checker__load_content_json_success_path(
    FileCheckerInstanceJSON_path,
    json_content_expected,
):
    FileCheckerInstanceJSON_path._load_content()
    assert_that(FileCheckerInstanceJSON_path._content).is_equal_to(
        json_content_expected
    )


def test_file_checker__load_content_fail_file_suffix_not_implemented_file(
    FileCheckerInstanceYAML_file,
):
    FileCheckerInstanceYAML_file._file_suffix = ".md"
    with pytest.raises(NotImplementedError):
        FileCheckerInstanceYAML_file._load_content()


def test_file_checker__load_content_fail_file_suffix_not_implemented_path(
    FileCheckerInstanceYAML_path,
):
    FileCheckerInstanceYAML_path._file_suffix = ".md"
    with pytest.raises(NotImplementedError):
        FileCheckerInstanceYAML_path._load_content()
