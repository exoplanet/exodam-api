# Test imports
from assertpy import assert_that, soft_assertions

# Third party imports
from fastapi import status


def test_api__add(client):
    response = client.get("/exodam/api/add")
    expected_json = {
        "details": "Adding verbe available via the API. "
        "These verbes change the state of the server.",
        "data": {"avaible_adding_verbes": {"news": "Add new(s) in the database."}},
    }

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(response.json()).is_equal_to(expected_json)
