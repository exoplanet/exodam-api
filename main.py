"""Exodam API."""

# Standard imports
from json import JSONDecodeError
from pathlib import Path as pyPath
from typing import Annotated

# Third party imports
from dotenv import load_dotenv

# Exodam
from exodam import (
    ExodamContent,
    ExodamError,
    ExodictType,
    ScientificGenerateParams,
    diff,
    dog_house,
    generate_and_insert_exodict,
    generate_cerberus_and_insert,
    get_yml_skel,
)
from exodam_resources import (
    generate_and_insert_exodam_types,
    generate_and_insert_known_keys,
    generate_and_insert_vupnu_keys,
    generate_and_insert_yml_skel,
)

# TODO: import from exodam when this issue is solve:
# https://gitlab.obspm.fr/exoplanet/exodam/-/issues/119
# TODO: import from exodam when this issue is solve:
# https://gitlab.obspm.fr/exoplanet/exodam/-/issues/119
from exodict import get_exodict
from fastapi import FastAPI, File, Form, Request, UploadFile, status
from fastapi.responses import HTMLResponse, JSONResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from py_linq_sql import PyLinqSQLError, SQLEnumerable
from yaml import YAMLError

# First party imports
from exodam_api import (
    ExodamResponseError,
    ExodamResponseVersion,
    ExodamResponseWithData,
    FileChecker,
    SkelType,
    __version__,
    clean_none_data,
    convert_exodam,
    get_conn,
)

app = FastAPI()
BASE_DIRE = pyPath(__file__).parent
app.mount("/static", StaticFiles(directory=f"{BASE_DIRE}/static"), name="static")
templates = Jinja2Templates(directory=f"{BASE_DIRE}/templates")
load_dotenv()


# Redirect path for tests. This is not tested
@app.get("/")
async def root() -> HTMLResponse:
    """
    Redirection to `/exodam/`.

    Get html form page for the API.
    """
    return RedirectResponse(url="/exodam/")  # pragma: no cover


# Redirect path for tests. This is not tested
@app.get("/home/")
async def home() -> HTMLResponse:
    """
    Redirection to `/exodam/`.

    Get html form page for the API.
    """
    return RedirectResponse(url="/exodam/")  # pragma: no cover


# TODO: Should be a django view of exoplanet.eu
# This function is not tested. It is just used for testing before the api is
# "online" with django.
@app.get("/exodam/", response_class=HTMLResponse)
async def exodam(request: Request) -> HTMLResponse:
    """Get html form page for the API."""
    return templates.TemplateResponse(  # pragma: no cover
        "index.html.j2",
        {"request": request, "types": SkelType.as_str_lst()},
    )


@app.get("/exodam/api", response_model=ExodamResponseVersion)
async def version() -> ExodamResponseVersion:
    """Get information ressource on the API."""
    return {
        "details": "EXODAM-API is an API to help in the creation and manipulation of "
        "exoplanets and other exo-objects (exomoons, discs, comets...) "
        "description files.",
        "version": __version__,
    }


@app.get(
    "/exodam/api/skel/",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_501_NOT_IMPLEMENTED: {"model": ExodamResponseError}},
)
async def get_skeleton(
    sk_type: SkelType,
) -> ExodamResponseWithData | ExodamResponseError:
    """Get a skeleton ressource of an exodam file."""
    if sk_type is SkelType.YAML or sk_type is SkelType.YML:
        return {
            "details": "A json/yaml skeleton file to create an exodam file.",
            "data": get_yml_skel(ExodictType.SCIENTIFIC),
        }
    elif sk_type is SkelType.CSV:
        return JSONResponse(
            status_code=status.HTTP_501_NOT_IMPLEMENTED,
            content={
                "details": "Not implemented feature.",
                "warnings": [
                    "CSV skeleton is not yet implemented.",
                    "Check Readme, Changelog and issues of the exodam project for more "
                    "informations",
                ],
            },
        )


@app.get("/exodam/api/actions/", response_model=ExodamResponseWithData)
async def actions() -> ExodamResponseWithData:
    """Get actions ressource informations."""
    return {
        "details": "All different actions/services available via the API. "
        "These services do not relate directly to a resource. They don't change "
        "the state of the server, but have side effects.",
        "data": {
            "avaible_actions": {
                "diff": "Does not change the server state, but generate diff between "
                "two yaml file as a side effect",
                "check": "Does not change the server state, but generates a check of a "
                "yaml file as a side effect.",
                "scientific-convert-exodam": "Does not change the server state, but "
                "convert a scientific yaml file in an exodam file as a side effect.",
                "editorial-convert-exodam": "Does not change the server state, but "
                "convert a editorial yaml file in an exodam file as a side effect.",
            }
        },
    }


@app.post(
    "/exodam/api/actions/diff/",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: {"model": ExodamResponseError}},
)
async def diff_files(
    file_1: Annotated[UploadFile, File(description="First file to make the diff")],
    file_2: Annotated[UploadFile, File(description="Second file to make the diff")],
) -> ExodamResponseWithData | ExodamResponseError:
    """
    Do not change the server state.

    But generate diff between two yaml file as a side effect.
    """
    file_1_path = pyPath(file_1.filename)
    file_2_path = pyPath(file_2.filename)

    try:
        content_1 = FileChecker(file_1).is_yaml().is_empty().content_or_raise()
        content_2 = FileChecker(file_2).is_yaml().is_empty().content_or_raise()
    except (YAMLError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )
    finally:
        file_1.file.close()
        file_2.file.close()

    result = diff(ExodamContent(content_1), ExodamContent(content_2))

    return {
        "details": "No differences" if result == "" else "Differences found",
        "data": {
            "files": [file_1_path, file_2_path],
            "diff": not bool(result),
            "diff_str": result,
        },
    }


@app.post(
    "/exodam/api/actions/check/",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: {"model": ExodamResponseError}},
)
async def check_file(
    file_to_check: Annotated[UploadFile, File(description="File to check")],
    permissive: bool = Form(False),
    with_doi: bool = Form(False),
) -> ExodamResponseWithData | ExodamResponseError:
    """
    Do not change the server state.

    But generates a check of a yaml file as a side effect.
    """
    file_path = pyPath(file_to_check.filename)
    try:
        content = FileChecker(file_to_check).is_yaml().is_empty().content_or_raise()
    except (YAMLError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )
    finally:
        file_to_check.file.close()

    warning_msg = []
    result = dog_house(
        ExodamContent(content),
        ExodictType.SCIENTIFIC,
        permissive,
        warning_msg,
        with_doi,
        color=False,
    )

    return {
        "details": "Is valid file" if result else "Is not a valid file",
        "data": {
            "file": file_path,
            "options": {"permissive": permissive, "with_doi": with_doi},
            "check": result,
            "warning_msg": warning_msg,
        },
    }


@app.post(
    "/exodam/api/actions/scientific-convert-exodam",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError}},
)
async def scientific_finalize(
    file_to_convert: Annotated[UploadFile, File(description="File to convert")],
) -> ExodamResponseWithData | ExodamResponseError:
    """
    Do not change the server state.

    But convert a scientific YAML file in an exodam file as a side effect.
    """
    return convert_exodam(file_to_convert, ExodictType.SCIENTIFIC)


@app.post(
    "/exodam/api/actions/editorial-convert-exodam",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError}},
)
async def editorial_finalize(
    file_to_convert: Annotated[UploadFile, File(description="File to convert")],
) -> ExodamResponseWithData | ExodamResponseError:
    """
    Do not change the server state.

    But convert an editorial YAML file in an exodam file as a side effect.
    """
    return convert_exodam(file_to_convert, ExodictType.EDITORIAL)


@app.get("/exodam/api/add/", response_model=ExodamResponseWithData)
async def add() -> ExodamResponseWithData:
    """Get add ressource informations."""
    return {
        "details": "Adding verbe available via the API. "
        "These verbes change the state of the server.",
        "data": {"avaible_adding_verbes": {"news": "Add new(s) in the database."}},
    }


@app.post(
    "/exodam/api/add/news",
    response_model=ExodamResponseWithData,
    responses={
        status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError},
        status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: {"model": ExodamResponseError},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"model": ExodamResponseError},
    },
)
async def add_news(
    news_to_add: Annotated[UploadFile, File(description="File of news to add")],
) -> ExodamResponseError | ExodamResponseWithData:
    """Add a news ressource."""
    file_path = pyPath(news_to_add.filename)
    try:
        content = FileChecker(news_to_add).is_json().is_empty().content_or_raise()
    except (JSONDecodeError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )
    finally:
        news_to_add.file.close()

    exodict_version = content.get("exodict_version", None)
    exodict = content.get("exodict", None)
    data = content.get("data", None)
    news = data.get("news", None) if data is not None else None

    missing = [
        key
        for key, value in {
            "exodict_version": exodict_version,
            "exodict": exodict,
            "data": data,
            "news": news,
        }.items()
        if value is None
    ]

    if missing:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content={
                "details": "Unprocessable Entity",
                "warnings": [
                    "Given file doesn't contains all mandatory information.",
                    "We need an exodict, an exodict_version and data.",
                    f"Missing: {', '.join(missing)}",
                ],
            },
        )

    cleaned_news = clean_none_data(news)
    insert_data = [(clean_news, exodict_version) for clean_news in cleaned_news]

    conn = get_conn()

    try:
        sqle = (
            SQLEnumerable(conn, "exodam.editorial_exodam_news")
            .insert(["data", "exodict_version"], insert_data)
            .execute()
        )
    except PyLinqSQLError as err:
        conn.close()
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "details": "Internal Server Error",
                "warnings": [
                    "Something went wrong when we try to insert news."
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    if sqle == len(insert_data):
        conn.commit()
        conn.close()
        return {
            "details": f"All ({sqle}) news has been inserted. All good",
            "data": {"file": file_path, "inserted_data": cleaned_news},
        }

    # No cover because it's just a security
    conn.close()  # pragma: no cover
    return JSONResponse(  # pragma: no cover
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            "details": "Aborted requests",
            "warnings": [
                "Something went wrong when we try to insert news."
                "We abort the insertion. Try again later or contact admin.",
            ],
        },
    )


# TODO Change to take Editorial Exodict
@app.post(
    "/exodam/api/exodict/",
    response_model=ExodamResponseWithData,
    responses={
        status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError},
        status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: {"model": ExodamResponseError},
    },
)
# PLR0913: Too many arguments to function call
async def add_exodict(  # noqa: PLR0913
    exo_type: Annotated[
        UploadFile | None,
        File(description="File for Exo types ressource. Null for no new ressource."),
    ] = None,
    identification: Annotated[
        UploadFile | None,
        File(
            description="File for identification ressource. Null for no new ressource."
        ),
    ] = None,
    internal: Annotated[
        UploadFile | None,
        File(description="File for internal ressource. Null for no new ressource."),
    ] = None,
    parameters: Annotated[
        UploadFile | None,
        File(description="File for parameters ressource. Null for no new ressource."),
    ] = None,
    reference: Annotated[
        UploadFile | None,
        File(description="File for references ressource. Null for no new ressource."),
    ] = None,
    relationship: Annotated[
        UploadFile | None,
        File(
            description="File for relationships ressource. Null for no new ressource."
        ),
    ] = None,
) -> ExodamResponseWithData | ExodamResponseError:
    """Add am exodict ressource."""
    saved_args = locals()

    paths = {
        arg_name: pyPath(arg_value.filename) if arg_value is not None else None
        for arg_name, arg_value in saved_args.items()
    }

    if all(path is None for path in paths.values()):
        return {
            "details": "No new file given. We do nothing. 0 new files, 6 reference "
            "files.",
            "data": {
                "reference_files": paths,
                "new_files": {},
            },
        }

    try:
        contents = {}
        for arg_name, arg_value in saved_args.items():
            if arg_value is not None:
                contents[arg_name] = (
                    FileChecker(arg_value).is_yaml().is_empty().content_or_raise()
                )
    except (JSONDecodeError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    generate_params = ScientificGenerateParams(
        exo_type=contents.get("exo_type", None),
        identification=contents.get("identification", None),
        internal=contents.get("internal", None),
        parameters=contents.get("parameters", None),
        reference=contents.get("reference", None),
        relationship=contents.get("relationship", None),
    )

    try:
        # Generate and insert the exodict
        generate_and_insert_exodict(generate_params, ExodictType.SCIENTIFIC)

        # Generate and insert ressources
        generate_and_insert_exodam_types()
        generate_and_insert_known_keys(ExodictType.SCIENTIFIC)
        generate_and_insert_vupnu_keys()
        generate_and_insert_yml_skel(ExodictType.SCIENTIFIC)
        generate_cerberus_and_insert(ExodictType.SCIENTIFIC)

        # Get the new exodict
        final_exodict = get_exodict(ExodictType.SCIENTIFIC)
    except (PyLinqSQLError, ExodamError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "details": "Internal Server Error",
                "warnings": [
                    "Something went wrong when we try to insert news."
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    files = {
        "exo_type": paths["exo_type"],
        "identification": paths["identification"],
        "internal": paths["internal"],
        "parameters": paths["parameters"],
        "reference": paths["reference"],
        "relationship": paths["relationship"],
    }
    reference_files = {key: value for key, value in files.items() if value is None}
    new_files = {key: value for key, value in files.items() if value is not None}

    return {
        "details": f"New exodict has been inserted without problems. {len(new_files)} "
        f"new files, {len(reference_files)} reference files.",
        "data": {
            "reference_files": reference_files,
            "new_files": new_files,
            "final_exodict": final_exodict,
        },
    }


@app.post(
    "/exodam/api/disk",
    response_model=ExodamResponseWithData,
    responses={
        status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError},
        status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: {"model": ExodamResponseError},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"model": ExodamResponseError},
    },
)
async def add_disk(
    disk_to_add: Annotated[UploadFile, File(description="File of disks to add")],
) -> ExodamResponseError | ExodamResponseWithData:
    """Add a disk ressource."""
    file_path = pyPath(disk_to_add.filename)
    try:
        content = FileChecker(disk_to_add).is_json().is_empty().content_or_raise()
    except (JSONDecodeError, ValueError) as err:
        return JSONResponse(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            content={
                "details": "Unsupported Media Type",
                "warnings": [
                    "Something went wrong when we try to read files.",
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )
    finally:
        disk_to_add.file.close()

    exodict_version = content.get("exodict_version", None)
    exodict = content.get("exodict", None)
    data = content.get("data", None)
    objects = data.get("objects", None) if data is not None else None

    missing = [
        key
        for key, value in {
            "exodict_version": exodict_version,
            "exodict": exodict,
            "data": data,
            "objects": objects,
        }.items()
        if value is None
    ]

    if missing:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content={
                "details": "Unprocessable Entity",
                "warnings": [
                    "Given file doesn't contains all mandatory information.",
                    "We need an exodict, an exodict_version and data.",
                    f"Missing: {', '.join(missing)}",
                ],
            },
        )

    cleaned_objects = clean_none_data(objects)
    insert_data = [(clean_news, exodict_version) for clean_news in cleaned_objects]

    conn = get_conn()

    try:
        sqle = (
            SQLEnumerable(conn, "exodam.disk")
            .insert(["data", "exodict_version"], insert_data)
            .execute()
        )
    except PyLinqSQLError as err:
        conn.close()
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "details": "Internal Server Error",
                "warnings": [
                    "Something went wrong when we try to insert disks."
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    if sqle == len(insert_data):
        conn.commit()
        conn.close()
        return {
            "details": f"All ({sqle}) disk(s) has been inserted. All good",
            "data": {"file": file_path, "inserted_data": cleaned_objects},
        }

    # No cover because it's just a security
    conn.close()  # pragma: no cover
    return JSONResponse(  # pragma: no cover
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            "details": "Aborted requests",
            "warnings": [
                "Something went wrong when we try to insert disks."
                "We abort the insertion. Try again later or contact admin.",
            ],
        },
    )


@app.get(
    "/exodam/api/exodict/scientific",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError}},
)
async def get_scientific_exodict() -> ExodamResponseWithData | ExodamResponseError:
    """Get the reference scientific exodict in JSON format."""
    try:
        ref_exodict = get_exodict(ExodictType.SCIENTIFIC)
    except (ValueError, PyLinqSQLError) as err:
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "details": "Internal Server Error",
                "warnings": [
                    "Something went wrong when when retrieving exodict. "
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=ref_exodict,
    )


@app.get(
    "/exodam/api/exodict/editorial",
    response_model=ExodamResponseWithData,
    responses={status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": ExodamResponseError}},
)
async def get_editorial_exodict() -> ExodamResponseWithData | ExodamResponseError:
    """Get the reference editorial exodict in JSON format."""
    try:
        ref_exodict = get_exodict(ExodictType.EDITORIAL)
    except (ValueError, PyLinqSQLError) as err:
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "details": "Internal Server Error",
                "warnings": [
                    "Something went wrong when when retrieving exodict. "
                    f"Exception TYPE: {type(err)}",
                    f"{err!s}",
                ],
            },
        )

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=ref_exodict,
    )
