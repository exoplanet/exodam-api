"""Clean data functions fro the API."""

# Standard imports
from typing import Any

# Third party imports
from flatten_dict import flatten, unflatten
from flatten_dict.reducers import make_reducer
from flatten_dict.splitters import make_splitter

# TODO: Get from a config file
DICT_FLATTEN_DELIMITER = "_!_&_"


def clean_none_data(data: list[dict[str, Any]]) -> list[dict[str, Any]]:
    """
    Clean None in data.

    Remove all key with a value equal to `None`.

    Args:
        data: Data to clean.

    Returns:
        Cleaned data.
    """
    result = []
    for element in data:
        res = {}
        flatten_element = flatten(element, reducer=make_reducer(DICT_FLATTEN_DELIMITER))
        for key, value in flatten_element.items():
            if value is not None:
                res[key] = value
        result.append(
            unflatten(res, splitter=make_splitter(delimiter=DICT_FLATTEN_DELIMITER))
        )
    return result
