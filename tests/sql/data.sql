-- INSERT data INTO exodam_skel
INSERT INTO exodam.exodam_skel (refereed, data) VALUES
(
    True,
    '{
        "identity": {
            "doi": null,
            "name": null,
            "exo_type": null,
            "alternate_names": null
        },
        "internal": {
            "object_status": null,
            "display_status": null,
            "embargo_end_date": null
        },
        "parameters": {
            "orbital":
            {
                "omega": null,
                "period": null,
                "angular_dis": null,
                "inclination": null,
                "eccentricity": null,
                "semi_major_axis": null
            },
            "physical": {
                "age": null,
                "logg": null,
                "mass": null,
                "teff": null,
                "radius": null,
                "t_calc": null,
                "t_meas": null,
                "density": null,
                "metallicity": null,
                "detected_disc": null,
                "spectral_type": null,
                "magnetic_field": null,
                "rotation_period": null
            },
            "position": {
                "ra": null,
                "dec": null,
                "distance": null,
                "pro_motion": null,
                "pro_motion_ra": null,
                "pro_motion_dec": null,
                "radial_velocity": null
            },
            "magnitude": {
                "magnitude_h": null,
                "magnitude_i": null,
                "magnitude_j": null,
                "magnitude_k": null,
                "magnitude_v": null
            },
            "atmospheric": {
                "temp": {
                    "temp_fig": null,
                    "temp_note": null,
                    "temp_type": null,
                    "temp_result": null,
                    "temp_source": null
                },
                "albedo": null,
                "hot_pt": null,
                "molecule": null
            },
            "specific_planet_parameter": {
                "k": null,
                "t_0": null,
                "tvr": null,
                "t0_sec": null,
                "t_conj": null,
                "t_peri": null,
                "lambda_ang": null,
                "impact_param": null,
                "rad_meas_method": null,
                "detection_method": null,
                "discovery_method": null,
                "mass_meas_method": null
            }
        },
        "references": {
            "links": null,
            "url_simbad": null,
            "publication": {
                "doi": null,
                "ref": null,
                "url": null,
                "date": null,
                "title": null,
                "author": null,
                "keyword": null,
                "bib_code": null,
                "journal_name": null,
                "journal_page": null,
                "journal_volume": null,
                "publication_type": null,
                "publication_status": null
            }
        },
        "relationship": {
            "gravitational_system": null
        }
    }'
);

-- INSERT data into exodam_types table
INSERT INTO exodam.exodam_types (refereed, data) VALUES
(
    True,
    '{
        "exodam_types": [
            "star",
            "planet",
            "satellite",
            "small_body",
            "pulsar",
            "ring",
            "disk"
        ]
    }'
);

-- INSERT data into exodam_known_keys table
INSERT INTO exodam.exodam_known_keys (refereed, data) VALUES
(
    True,
    '{
        "known_keys": [
            "label",
            "short",
            "long",
            "full_ascii",
            "value",
            "unit",
            "uncert",
            "pos_uncert",
            "neg_uncert",
            "objects_type",
            "relationship",
            "units",
            "objects",
            "identity",
            "parameters",
            "references",
            "internal",
            "doi",
            "name",
            "alternate_names",
            "exo_type",
            "object_status",
            "embargo_end_date",
            "display_status",
            "publication",
            "links",
            "url_simbad",
            "title",
            "author",
            "date",
            "publication_type",
            "journal_name",
            "journal_volume",
            "journal_page",
            "bib_code",
            "keyword",
            "ref",
            "doi",
            "url",
            "publication_status",
            "position",
            "magnitude",
            "orbital",
            "physical",
            "specific_planet_parameter",
            "atmospheric",
            "distance",
            "ra",
            "dec",
            "pro_motion",
            "pro_motion_ra",
            "pro_motion_dec",
            "radial_velocity",
            "magnitude_v",
            "magnitude_h",
            "magnitude_k",
            "magnitude_j",
            "magnitude_i",
            "period",
            "semi_major_axis",
            "eccentricity",
            "inclination",
            "omega",
            "angular_dis",
            "mass",
            "radius",
            "density",
            "rotation_period",
            "logg",
            "t_calc",
            "t_meas",
            "spectral_type",
            "metallicity",
            "teff",
            "magnetic_field",
            "detected_disc",
            "age",
            "detection_method",
            "discovery_method",
            "t_peri",
            "t_conj",
            "t_0",
            "t0_sec",
            "lambda_ang",
            "impact_param",
            "tvr",
            "k",
            "mass_meas_method",
            "rad_meas_method",
            "hot_pt",
            "albedo",
            "temp",
            "molecule",
            "temp_type",
            "temp_note",
            "temp_source",
            "temp_result",
            "temp_fig",
            "molecule_type",
            "molecule_name",
            "molecule_note",
            "molecule_result",
            "molecule_source",
            "molecule_fig",
            "gravitational_system",
            "name",
            "symbol",
            "wiki_data_rdf"
        ]
    }'
);

-- INSERT data into exodam_vupnu_keys table
INSERT INTO exodam.exodam_vupnu_keys (refereed, data) VALUES
(
    True,
    '{
        "vupnu_keys": [
            "position__distance",
            "position__ra",
            "position__dec",
            "position__pro_motion",
            "position__pro_motion_ra",
            "position__pro_motion_dec",
            "position__radial_velocity",
            "magnitude__magnitude_v",
            "magnitude__magnitude_h",
            "magnitude__magnitude_k",
            "magnitude__magnitude_j",
            "magnitude__magnitude_i",
            "orbital__period",
            "orbital__semi_major_axis",
            "orbital__eccentricity",
            "orbital__inclination",
            "orbital__omega",
            "orbital__angular_dis",
            "physical__mass",
            "physical__radius",
            "physical__density",
            "physical__rotation_period",
            "physical__logg",
            "physical__t_calc",
            "physical__t_meas",
            "physical__metallicity",
            "physical__teff",
            "physical__age",
            "specific_planet_parameter__t_peri",
            "specific_planet_parameter__t_conj",
            "specific_planet_parameter__t_0",
            "specific_planet_parameter__t0_sec",
            "specific_planet_parameter__lambda_ang",
            "specific_planet_parameter__impact_param",
            "specific_planet_parameter__tvr",
            "specific_planet_parameter__k",
            "atmospheric__hot_pt",
            "atmospheric__albedo"
        ]
    }'
);


-- INSERT data into exodam_cerberus_data table
INSERT INTO exodam.exodam_cerberus_data (data) VALUES
(
'{
    "units": {
        "type": "dict",
        "keysrules": {
            "type": "string",
            "regex": "^[a-zA-Z]+[a-zA-Z_]+_(unit|UNIT)$"
        },
        "valuesrules": {
            "type": "list",
            "schema": {
                "type": "dict",
                "schema": {
                    "name": {
                        "type": "string",
                        "required": true
                    },
                    "symbol": {
                        "type": "list",
                        "required": true,
                        "schema": {
                        "type": "string"
                        }
                    },
                    "wiki_data_rdf": {
                        "type": "string",
                        "required": true,
                        "nullable": true
                    }
                }
            }
        }
    }
}'
),
('{
    "relationship": {
        "type": "dict",
        "required": true,
        "nullable": true,
        "schema": {
            "gravitational_system": {
                "type": "dict",
                "keysrules": {
                    "type": "string",
                    "regex": "^gs[0-9]+$"
                },
                "valuesrules": {
                    "type": "list",
                    "schema": {
                        "type": "string"
                    }
                }
            }
        }
    }
}'),
('{
    "objects_type": {
        "type": "list",
        "required": true,
        "schema": {
            "type": "string",
            "anyof": [
                {
                    "type": "string",
                    "regex": "star"
                },
                {
                    "type": "string",
                    "regex": "small_body"
                },
                {
                    "type": "string",
                    "regex": "pulsar"
                },
                {
                    "type": "string",
                    "regex": "ring"
                },
                {
                    "type": "string",
                    "regex": "disk"
                },
                {
                    "type": "string",
                    "regex": "planet"
                },
                {
                    "type": "string",
                    "regex": "satellite"
                }
            ]
        }
    }
}'),
('{
    "objects": {
        "type": "list",
        "required": true,
        "schema": {
            "type": "dict",
            "schema": {
                "identity": {
                    "type": "dict",
                    "required": true
                },
                "parameters": {
                    "type": "dict",
                    "required": true
                },
                "references": {
                    "type": "dict",
                    "required": true
                },
                "internal": {
                    "type": "dict"
                }
            }
        }
    }
}'),
('{
    "objects_type": {
        "type": "list",
        "required": true,
        "schema": {
        "type": "string",
        "anyof": [
            {
            "type": "string",
            "regex": "star"
            },
            {
            "type": "string",
            "regex": "small_body"
            },
            {
            "type": "string",
            "regex": "pulsar"
            },
            {
            "type": "string",
            "regex": "ring"
            },
            {
            "type": "string",
            "regex": "disk"
            },
            {
            "type": "string",
            "regex": "planet"
            },
            {
            "type": "string",
            "regex": "satellite"
            }
        ]
        }
    },
    "objects": {
        "type": "list",
        "required": true,
        "schema": {
        "type": "dict",
        "schema": {
            "identity": {
            "type": "dict",
            "required": true,
            "schema": {
                "doi": {
                "type": "string",
                "required": true
                },
                "name": {
                "type": "string",
                "required": true
                },
                "alternate_names": {
                "type": "list",
                "schema": {
                    "type": "string"
                }
                },
                "exo_type": {
                "type": "string",
                "required": true,
                "oneof": [
                    {
                    "type": "string",
                    "regex": "star"
                    },
                    {
                    "type": "string",
                    "regex": "small_body"
                    },
                    {
                    "type": "string",
                    "regex": "pulsar"
                    },
                    {
                    "type": "string",
                    "regex": "ring"
                    },
                    {
                    "type": "string",
                    "regex": "disk"
                    },
                    {
                    "type": "string",
                    "regex": "planet"
                    },
                    {
                    "type": "string",
                    "regex": "satellite"
                    }
                ]
                }
            }
            }
        }
        }
    },
    "relationship": {
        "type": "dict",
        "required": true,
        "nullable": true,
        "schema": {
        "gravitational_system": {
            "type": "dict",
            "keysrules": {
            "type": "string",
            "regex": "^gs[0-9]+$"
            },
            "valuesrules": {
            "type": "list",
            "schema": {
                "type": "string"
            }
            }
        }
        }
    }
}'),
('{
    "references": {
        "required": true,
        "type": "dict",
        "schema": {
        "publication": {
            "required": true,
            "type": "dict",
            "schema": {
            "title": {
                "type": "string"
            },
            "author": {
                "type": "string"
            },
            "date": {
                "type": "string"
            },
            "publication_type": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "book"
                },
                {
                    "type": "string",
                    "regex": "thesis"
                },
                {
                    "type": "string",
                    "regex": "refereed article"
                },
                {
                    "type": "string",
                    "regex": "proceeding"
                },
                {
                    "type": "string",
                    "regex": "unknown"
                },
                {
                    "type": "string",
                    "regex": "report"
                }
                ]
            },
            "journal_name": {
                "type": "string"
            },
            "journal_volume": {
                "type": "string"
            },
            "journal_page": {
                "type": "string"
            },
            "bib_code": {
                "type": "string"
            },
            "keyword": {
                "type": "list",
                "schema": {
                "type": "string"
                }
            },
            "ref": {
                "type": "string"
            },
            "doi": {
                "type": "string",
                "required": true,
                "nullable": true
            },
            "url": {
                "type": "string",
                "required": true,
                "nullable": true
            },
            "publication_status": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "R"
                },
                {
                    "type": "string",
                    "regex": "S"
                },
                {
                    "type": "string",
                    "regex": "C"
                },
                {
                    "type": "string",
                    "regex": "W"
                }
                ]
            }
            }
        },
        "links": {
            "type": "list",
            "schema": {
            "type": "string"
            }
        },
        "url_simbad": {
            "type": "string"
        }
        }
    }
}'),
('{
    "Vupnu": {
        "type": [
        "float",
        "dict"
        ],
        "nullable": true,
        "schema": {
        "value": {
            "type": "float"
        },
        "uncert": {
            "type": "float"
        },
        "pos_uncert": {
            "type": "float"
        },
        "neg_uncert": {
            "type": "float"
        },
        "unit": {
            "type": "string"
        }
        }
    },
    "Methods": {
        "type": "string",
        "oneof": [
        {
            "type": "string",
            "regex": "RV"
        },
        {
            "type": "string",
            "regex": "timing"
        },
        {
            "type": "string",
            "regex": "microlensing"
        },
        {
            "type": "string",
            "regex": "imaging"
        },
        {
            "type": "string",
            "regex": "primary transit"
        },
        {
            "type": "string",
            "regex": "secondary transit"
        },
        {
            "type": "string",
            "regex": "astrometry"
        },
        {
            "type": "string",
            "regex": "TTV"
        },
        {
            "type": "string",
            "regex": "disk kinematics"
        },
        {
            "type": "string",
            "regex": "other"
        }
        ]
    },
    "parameters": {
        "type": "dict",
        "required": true,
        "schema": {
        "position": {
            "type": "dict",
            "schema": {
            "distance": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "ra": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "dec": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "pro_motion": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "pro_motion_ra": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "pro_motion_dec": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "radial_velocity": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            }
            }
        },
        "magnitude": {
            "type": "dict",
            "schema": {
            "magnitude_v": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "magnitude_h": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "magnitude_k": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "magnitude_j": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "magnitude_i": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            }
            }
        },
        "orbital": {
            "type": "dict",
            "schema": {
            "period": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "semi_major_axis": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "eccentricity": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "inclination": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "omega": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "angular_dis": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            }
            }
        },
        "physical": {
            "type": "dict",
            "schema": {
            "mass": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "radius": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "density": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "rotation_period": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "logg": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "t_calc": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "t_meas": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "spectral_type": {
                "type": "string"
            },
            "metallicity": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "teff": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "magnetic_field": {
                "type": "boolean"
            },
            "detected_disc": {
                "type": "boolean"
            },
            "age": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            }
            }
        },
        "specific_planet_parameter": {
            "type": "dict",
            "schema": {
            "detection_method": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "RV"
                },
                {
                    "type": "string",
                    "regex": "timing"
                },
                {
                    "type": "string",
                    "regex": "microlensing"
                },
                {
                    "type": "string",
                    "regex": "imaging"
                },
                {
                    "type": "string",
                    "regex": "primary transit"
                },
                {
                    "type": "string",
                    "regex": "secondary transit"
                },
                {
                    "type": "string",
                    "regex": "astrometry"
                },
                {
                    "type": "string",
                    "regex": "TTV"
                },
                {
                    "type": "string",
                    "regex": "disk kinematics"
                },
                {
                    "type": "string",
                    "regex": "other"
                }
                ]
            },
            "discovery_method": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "RV"
                },
                {
                    "type": "string",
                    "regex": "timing"
                },
                {
                    "type": "string",
                    "regex": "microlensing"
                },
                {
                    "type": "string",
                    "regex": "imaging"
                },
                {
                    "type": "string",
                    "regex": "primary transit"
                },
                {
                    "type": "string",
                    "regex": "secondary transit"
                },
                {
                    "type": "string",
                    "regex": "astrometry"
                },
                {
                    "type": "string",
                    "regex": "TTV"
                },
                {
                    "type": "string",
                    "regex": "disk kinematics"
                },
                {
                    "type": "string",
                    "regex": "other"
                }
                ]
            },
            "t_peri": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "t_conj": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "t_0": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "t0_sec": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "lambda_ang": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "impact_param": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "tvr": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "k": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "mass_meas_method": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "RV"
                },
                {
                    "type": "string",
                    "regex": "microlensing"
                },
                {
                    "type": "string",
                    "regex": "timing"
                },
                {
                    "type": "string",
                    "regex": "controversial"
                },
                {
                    "type": "string",
                    "regex": "astrometry"
                },
                {
                    "type": "string",
                    "regex": "ttv"
                },
                {
                    "type": "string",
                    "regex": "spectrum"
                },
                {
                    "type": "string",
                    "regex": "theoretical"
                }
                ]
            },
            "rad_meas_method": {
                "type": "string",
                "oneof": [
                {
                    "type": "string",
                    "regex": "primary transit"
                },
                {
                    "type": "string",
                    "regex": "theoretical"
                },
                {
                    "type": "string",
                    "regex": "flux"
                }
                ]
            }
            }
        },
        "atmospheric": {
            "type": "dict",
            "schema": {
            "hot_pt": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "albedo": {
                "type": [
                "float",
                "dict"
                ],
                "nullable": true,
                "schema": {
                "value": {
                    "type": "float"
                },
                "uncert": {
                    "type": "float"
                },
                "pos_uncert": {
                    "type": "float"
                },
                "neg_uncert": {
                    "type": "float"
                },
                "unit": {
                    "type": "string"
                }
                }
            },
            "temp": {
                "type": "dict",
                "schema": {
                "temp_type": {
                    "type": "string",
                    "oneof": [
                    {
                        "type": "string",
                        "regex": "disk_average"
                    },
                    {
                        "type": "string",
                        "regex": "day_average"
                    },
                    {
                        "type": "string",
                        "regex": "night_average"
                    },
                    {
                        "type": "string",
                        "regex": "spatial_variations"
                    },
                    {
                        "type": "string",
                        "regex": "vertical_profile"
                    },
                    {
                        "type": "string",
                        "regex": "3D_map"
                    },
                    {
                        "type": "string",
                        "regex": "hottest_point_longitude"
                    }
                    ]
                },
                "temp_note": {
                    "type": "string"
                },
                "temp_source": {
                    "type": "string",
                    "oneof": [
                    {
                        "type": "string",
                        "regex": "measurement"
                    },
                    {
                        "type": "string",
                        "regex": "modelling"
                    }
                    ]
                },
                "temp_result": {
                    "type": "string"
                },
                "temp_fig": {
                    "type": "string"
                }
                }
            },
            "molecule": {
                "type": "dict",
                "schema": {
                "molecule_type": {
                    "type": "string"
                },
                "molecule_name": {
                    "type": "list",
                    "schema": {
                    "type": "string"
                    }
                },
                "molecule_note": {
                    "type": "string"
                },
                "molecule_result": {
                    "type": "string"
                },
                "molecule_source": {
                    "type": "string",
                    "oneof": [
                    {
                        "type": "string",
                        "regex": "measurement"
                    },
                    {
                        "type": "string",
                        "regex": "modelling"
                    }
                    ]
                },
                "molecule_fig": {
                    "type": "string"
                }
                }
            }
            }
        }
        }
    }
}'),
('{
    "internal": {
        "type": "dict",
        "schema": {
        "object_status": {
            "type": "string",
            "oneof": [
            {
                "type": "string",
                "regex": "confirmed"
            },
            {
                "type": "string",
                "regex": "candidate"
            },
            {
                "type": "string",
                "regex": "controversial"
            },
            {
                "type": "string",
                "regex": "retracted"
            }
            ]
        },
        "embargo_end_date": {
            "type": "string"
        },
        "display_status": {
            "type": "boolean"
        }
        }
    }
}'),
('{
    "identity": {
        "type": "dict",
        "required": true,
        "schema": {
        "doi": {
            "type": "string",
            "required": true
        },
        "name": {
            "type": "string",
            "required": true
        },
        "alternate_names": {
            "type": "list",
            "schema": {
            "type": "string"
            }
        },
        "exo_type": {
            "type": "string",
            "required": true,
            "oneof": [
            {
                "type": "string",
                "regex": "star"
            },
            {
                "type": "string",
                "regex": "small_body"
            },
            {
                "type": "string",
                "regex": "pulsar"
            },
            {
                "type": "string",
                "regex": "ring"
            },
            {
                "type": "string",
                "regex": "disk"
            },
            {
                "type": "string",
                "regex": "planet"
            },
            {
                "type": "string",
                "regex": "satellite"
            }
            ]
        }
        }
    }
}');

-- ID REFERENCE
-- base -> units = 1
-- base -> relationship = 2
-- base -> objects_type = 3
-- base -> keys = 4
-- permissive -> permissive = 5
-- objects -> references = 6
-- objects -> parameters = 7
-- objects -> internal = 8
-- objects -> identity = 9

-- INSERT data into exodam_cerberus table
INSERT INTO exodam.exodam_cerberus (refereed) VALUES (True); -- id = 1

-- INSERT data into exodam_cerberus2data table
INSERT INTO exodam.exodam_cerberus2data (id_cerberus, name, cerberus_type, id_cerberus_data)
VALUES
(1, 'units', 'type__base', 1),
(1, 'relationship', 'type__base', 2),
(1, 'objects_type', 'type__base', 3),
(1, 'keys', 'type__base', 4),
(1, 'permissive', 'type__permissive', 5),
(1, 'references', 'type__objects', 6),
(1, 'parameters', 'type__objects', 7),
(1, 'internal', 'type__objects', 8),
(1, 'identity', 'type__objects', 9);

-- INSERT data into exodict_data table
INSERT INTO exodam.exodict_data (data) VALUES
('{
    "objects": {
    "star": {
        "definition": "astronomical object consisting of a luminous spheroid of plasma held together by its own gravity",
        "wiki_data_rdf": "Q523",
        "remark": "mass > 60 Mjup",
        "links": [],
        "label": {
        "short": null,
        "long": "star",
        "full_ascii": "star"
        }
    },
    "small_body": {
        "definition": "astronomical object smaller than a planet",
        "wiki_data_rdf": null,
        "remark": null,
        "links": [],
        "label": {
        "short": null,
        "long": "small body",
        "full_ascii": "small_body"
        }
    },
    "pulsar": {
        "definition": "highly magnetized, rapidly rotating neutron star",
        "wiki_data_rdf": "Q4360",
        "remark": null,
        "links": [],
        "label": {
        "short": null,
        "long": "pulsar",
        "full_ascii": "pulsar"
        }
    },
    "ring": {
        "definition": "ring of particles orbiting a planet or a small body",
        "wiki_data_rdf": "Q179792",
        "remark": null,
        "links": [],
        "label": {
        "short": null,
        "long": "ring",
        "full_ascii": "ring"
        }
    },
    "disk": {
        "definition": "circumstellar disk of gas or dust",
        "wiki_data_rdf": "Q3235978",
        "remark": null,
        "links": [],
        "label": {
        "short": null,
        "long": "disk",
        "full_ascii": "disk"
        }
    },
    "planet": {
        "definition": "any planet beyond the Solar System",
        "wiki_data_rdf": "Q44559",
        "remark": "the catalogue limit is 60 Jupiter mass for a planet",
        "links": [],
        "label": {
        "short": null,
        "long": "planet",
        "full_ascii": "planet"
        }
    },
    "satellite": {
        "definition": "celestial body that orbits a planet",
        "wiki_data_rdf": "Q2537",
        "remark": null,
        "links": [],
        "label": {
        "short": null,
        "long": "satellite",
        "full_ascii": "satellite"
        }
    }
    }
}'),
('{
    "identity": {
    "required": true,
    "doi": {
        "definition": "Serial code used to uniquely identify digital objects like academic papers",
        "wiki_data_rdf": "P356",
        "links": [
        "https://www.doi.org/"
        ],
        "label": {
        "short": "DOI",
        "long": "digital object identifier",
        "full_ascii": "doi"
        },
        "type": "str",
        "required": true
    },
    "name": {
        "definition": "object s name",
        "label": {
        "short": "name",
        "long": "object name",
        "full_ascii": "name"
        },
        "type": "str",
        "required": true
    },
    "alternate_names": {
        "definition": "object s alternate_names",
        "label": {
        "short": "alt. names",
        "long": "alternate object names",
        "full_ascii": "alternate_names"
        },
        "type": "list"
    },
    "exo_type": {
        "definition": "object s type",
        "label": {
        "short": null,
        "long": "exo type",
        "full_ascii": "exo_type"
        },
        "type": "string",
        "required": true,
        "oneof": [
        "star",
        "small_body",
        "pulsar",
        "ring",
        "disk",
        "planet",
        "satellite"
        ]
    }
    }
}'),
('{
    "internal": {
    "object_status": {
        "definition": "object status, must be in this list : confirmed, candidate, controversial, retracted",
        "label": {
        "short": "obj. status",
        "long": "object status",
        "full_ascii": "object_status"
        },
        "type": "str",
        "oneof": [
        "confirmed",
        "candidate",
        "controversial",
        "retracted"
        ]
    },
    "embargo_end_date": {
        "definition": "Date on which we can publish the object in the catalog",
        "label": {
        "short": null,
        "long": "embargo end date",
        "full_ascii": "embargo_end_date"
        },
        "type": "str"
    },
    "display_status": {
        "definition": "website page status",
        "label": {
        "short": null,
        "long": "display status",
        "full_ascii": "display_status"
        },
        "type": "bool"
    }
    }
}'),
('{
    "units": {
    "MASS_UNIT": [
        {
        "name": "jupiter mass",
        "symbol": [
            "M_J",
            "MJ",
            "mjupiter",
            "mass jupiter"
        ],
        "wiki_data_rdf": "Q651336"
        },
        {
        "name": "solar mass",
        "symbol": [
            "M☉",
            "mass of the Sun"
        ],
        "wiki_data_rdf": "Q180892"
        },
        {
        "name": "earth mass",
        "symbol": [
            "M⊕"
        ],
        "wiki_data_rdf": "Q681996"
        }
    ],
    "RADIUS_UNIT": [
        {
        "name": "jupiter radius",
        "symbol": [
            "RJ",
            "Rj"
        ],
        "wiki_data_rdf": "Q3421309"
        },
        {
        "name": "solar radius",
        "symbol": [
            "R☉"
        ],
        "wiki_data_rdf": "Q48440"
        },
        {
        "name": "earth radius",
        "symbol": [
            "terrestrial radius",
            "R⊕",
            "radius of the Earth"
        ],
        "wiki_data_rdf": "Q95689145"
        }
    ],
    "TIME_UNIT": [
        {
        "name": "hour",
        "symbol": "h",
        "wiki_data_rdf": "Q25235"
        },
        {
        "name": "year",
        "symbol": "y",
        "wiki_data_rdf": "Q577"
        },
        {
        "name": "day",
        "symbol": "day",
        "wiki_data_rdf": "Q573"
        },
        {
        "name": "second",
        "symbol": "s",
        "wiki_data_rdf": "Q11574"
        }
    ],
    "VELOCITY_UNIT": [
        {
        "name": "meter per second",
        "symbol": "m/s",
        "wiki_data_rdf": "Q182429"
        },
        {
        "name": "astronomical unit per year",
        "symbol": "au/yr",
        "wiki_data_rdf": "Q60742631"
        }
    ],
    "ANGLE_UNIT": [
        {
        "name": "degree",
        "symbol": "deg",
        "wiki_data_rdf": "Q28390"
        },
        {
        "name": "hms",
        "symbol": "hh:mm:ss",
        "wiki_data_rdf": "Q13442"
        },
        {
        "name": "dms",
        "symbol": "dd:mm:ss",
        "wiki_data_rdf": "Q76287"
        }
    ],
    "JULIAN_DAY_UNIT": [
        {
        "name": "Julian day",
        "symbol": "JD",
        "wiki_data_rdf": "Q14267"
        }
    ]
    },
    "parameters": {
    "required": true,
    "position": {
        "label": {
        "short": "pos.",
        "long": "position",
        "full_ascii": "position"
        },
        "distance": {
        "definition": "distance between the observer and the astronomical object",
        "unit": [
            {
            "name": "parsec",
            "symbol": "pc",
            "wiki_data_rdf": "Q12129"
            }
        ],
        "wiki_data_rdf": "Q847073",
        "label": {
            "short": "dist.",
            "long": "distance",
            "full_ascii": "distance"
        },
        "type": "vupnu"
        },
        "ra": {
        "definition": "astronomical equivalent of longitude: first spherical equatorial coordinate : Right Ascension",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "wiki_data_rdf": "Q13442",
        "label": {
            "short": "<math><msub><mi>&alpha;</mi><mn>2000</mn></msub></math>",
            "long": "right ascension",
            "full_ascii": "ra"
        },
        "type": "vupnu"
        },
        "dec": {
        "definition": "astronomical coordinate: second spherical equatorial coordinate : Declination",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "wiki_data_rdf": "Q76287",
        "label": {
            "short": "<math><msub><mi>&delta;</mi><mn>2000</mn></msub></math>",
            "long": "declination",
            "full_ascii": "dec"
        },
        "type": "vupnu"
        },
        "pro_motion": {
        "definition": "change in the place of a celestial body in the sky plane",
        "unit": [
            {
            "name": "milliarcseconds per year",
            "symbol": "mas/yr",
            "wiki_data_rdf": "Q22137107"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "pro motion",
            "long": "proper motion",
            "full_ascii": "pro_motion"
        },
        "type": "vupnu"
        },
        "pro_motion_ra": {
        "definition": "change in the place of a celestial body in the sky plane along the RA coordinate",
        "unit": [
            {
            "name": "milliarcseconds per year",
            "symbol": "mas/yr",
            "wiki_data_rdf": "Q22137107"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><ms>pro motion</ms><mo> </mo><mi>&alpha;</mi></math>",
            "long": "proper motion of the right ascension",
            "full_ascii": "pro_motion_ra"
        },
        "type": "vupnu"
        },
        "pro_motion_dec": {
        "definition": "change in the place of a celestial body in the sky plane along the Dec coordinate",
        "unit": [
            {
            "name": "milliarcseconds per year",
            "symbol": "mas/yr",
            "wiki_data_rdf": "Q22137107"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><ms>pro motion</ms><mo> </mo><mi>&delta;</mi></math>",
            "long": "proper motion of the declination",
            "full_ascii": "pro_motion_dec"
        },
        "type": "vupnu"
        },
        "radial_velocity": {
        "definition": "the speed with which the object moves away from the Earth (or approaches it, for a negative radial velocity).",
        "unit": [
            {
            "name": "meter per second",
            "symbol": "m/s",
            "wiki_data_rdf": "Q182429"
            }
        ],
        "wiki_data_rdf": "Q240105",
        "label": {
            "short": null,
            "long": "radial velocity",
            "full_ascii": "radial_velocity"
        },
        "type": "vupnu"
        }
    },
    "magnitude": {
        "label": {
        "short": "mag.",
        "long": "magnitude",
        "full_ascii": "magnitude"
        },
        "magnitude_v": {
        "definition": "apparent magnitude in v band",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q4892529",
        "label": {
            "short": "<math><msub><mi>m</mi><mn>V</mn></msub></math>",
            "long": "magnitude in V band",
            "full_ascii": "magnitude_v"
        },
        "type": "vupnu"
        },
        "magnitude_h": {
        "definition": "apparent magnitude in h band",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q16556693",
        "label": {
            "short": "<math><msub><mi>m</mi><mn>H</mn></msub></math>",
            "long": "magnitude in H band",
            "full_ascii": "magnitude_h"
        },
        "type": "vupnu"
        },
        "magnitude_k": {
        "definition": "apparent magnitude in k band",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q2520419",
        "label": {
            "short": "<math><msub><mi>m</mi><mn>K</mn></msub></math>",
            "long": "magnitude in K band",
            "full_ascii": "magnitude_k"
        },
        "type": "vupnu"
        },
        "magnitude_j": {
        "definition": "apparent magnitude in j band",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q15991308",
        "label": {
            "short": "<math><msub><mi>m</mi><mn>J</mn></msub></math>",
            "long": "magnitude in J band",
            "full_ascii": "magnitude_j"
        },
        "type": "vupnu"
        },
        "magnitude_i": {
        "definition": "apparent magnitude in i band",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q15987557",
        "label": {
            "short": "<math><msub><mi>m</mi><mn>I</mn></msub></math>",
            "long": "magnitude in I band",
            "full_ascii": "magnitude_i"
        },
        "type": "vupnu"
        }
    },
    "orbital": {
        "label": {
        "short": null,
        "long": "orbital",
        "full_ascii": "orbital"
        },
        "period": {
        "definition": "the time taken for a given astronomic object to make one complete orbit about another object",
        "unit": [
            {
            "name": "hour",
            "symbol": "h",
            "wiki_data_rdf": "Q25235"
            },
            {
            "name": "year",
            "symbol": "y",
            "wiki_data_rdf": "Q577"
            },
            {
            "name": "day",
            "symbol": "day",
            "wiki_data_rdf": "Q573"
            },
            {
            "name": "second",
            "symbol": "s",
            "wiki_data_rdf": "Q11574"
            }
        ],
        "wiki_data_rdf": "P2146",
        "label": {
            "short": null,
            "long": "period",
            "full_ascii": "period"
        },
        "type": "vupnu"
        },
        "semi_major_axis": {
        "definition": "longest interval from a point on an ellipse to its center",
        "unit": [
            {
            "name": "astronomical unit",
            "symbol": "ua",
            "wiki_data_rdf": "Q1811"
            }
        ],
        "wiki_data_rdf": "Q171594",
        "label": {
            "short": "<math><mi>a</mi></math>",
            "long": "semi-major axis",
            "full_ascii": "semi_major_axis"
        },
        "type": "vupnu"
        },
        "eccentricity": {
        "definition": "amount of the deviation of an orbit from a perfect circle",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q208474",
        "label": {
            "short": "<math><mi>e</mi></math>",
            "long": "eccentricity",
            "full_ascii": "eccentricity"
        },
        "type": "vupnu"
        },
        "inclination": {
        "definition": "angle between a reference plane and the plane of an orbit",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "wiki_data_rdf": "Q4112212",
        "label": {
            "short": "<math><mi>i</mi></math>",
            "long": "inclination",
            "full_ascii": "inclination"
        },
        "type": "vupnu"
        },
        "omega": {
        "definition": "the argument of periapsis is the angle from the body s ascending node to its periapsis, measured in the direction of motion",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><mi>&omega;</mi></math>",
            "long": "omega",
            "full_ascii": "omega"
        },
        "type": "vupnu"
        },
        "angular_dis": {
        "definition": "size of the angle between the two directions originating from the observer and pointing towards two objects",
        "unit": [
            {
            "name": "arcsecond",
            "symbol": "arcsec",
            "wiki_data_rdf": "Q829073"
            }
        ],
        "wiki_data_rdf": "P2212",
        "label": {
            "short": "<math><mi>&theta;</mi></math>",
            "long": "angular distance",
            "full_ascii": "angular_dis"
        },
        "type": "vupnu"
        }
    },
    "physical": {
        "label": {
        "short": "phy.",
        "long": "physical",
        "full_ascii": "physical"
        },
        "mass": {
        "definition": "property of matter to resist changes of the state of motion and to attract other bodies",
        "unit": [
            {
            "name": "jupiter mass",
            "symbol": [
                "M_J",
                "MJ",
                "mjupiter",
                "mass jupiter"
            ],
            "wiki_data_rdf": "Q651336"
            },
            {
            "name": "solar mass",
            "symbol": [
                "M☉",
                "mass of the Sun"
            ],
            "wiki_data_rdf": "Q180892"
            },
            {
            "name": "earth mass",
            "symbol": [
                "M⊕"
            ],
            "wiki_data_rdf": "Q681996"
            }
        ],
        "wiki_data_rdf": "Q11423",
        "label": {
            "short": null,
            "long": "mass",
            "full_ascii": "mass"
        },
        "type": "vupnu"
        },
        "radius": {
        "definition": "segment in a circle or sphere from its center to its perimeter or surface and its length",
        "unit": [
            {
            "name": "jupiter radius",
            "symbol": [
                "RJ",
                "Rj"
            ],
            "wiki_data_rdf": "Q3421309"
            },
            {
            "name": "solar radius",
            "symbol": [
                "R☉"
            ],
            "wiki_data_rdf": "Q48440"
            },
            {
            "name": "earth radius",
            "symbol": [
                "terrestrial radius",
                "R⊕",
                "radius of the Earth"
            ],
            "wiki_data_rdf": "Q95689145"
            }
        ],
        "wiki_data_rdf": "Q173817",
        "label": {
            "short": null,
            "long": "radius",
            "full_ascii": "radius"
        },
        "type": "vupnu"
        },
        "density": {
        "definition": "mass per volume",
        "unit": [
            {
            "name": "gram per cubic centimetre",
            "symbol": "g/cm³",
            "wiki_data_rdf": "Q13147228"
            }
        ],
        "wiki_data_rdf": "Q29539",
        "label": {
            "short": "<math><mi>&rho;</mi></math>",
            "long": "density",
            "full_ascii": "density"
        },
        "type": "vupnu"
        },
        "rotation_period": {
        "definition": "time that it takes to complete one revolution around its axis of rotation relative to the background stars",
        "unit": [
            {
            "name": "hour",
            "symbol": "h",
            "wiki_data_rdf": "Q25235"
            },
            {
            "name": "year",
            "symbol": "y",
            "wiki_data_rdf": "Q577"
            },
            {
            "name": "day",
            "symbol": "day",
            "wiki_data_rdf": "Q573"
            },
            {
            "name": "second",
            "symbol": "s",
            "wiki_data_rdf": "Q11574"
            }
        ],
        "wiki_data_rdf": "Q185981",
        "label": {
            "short": null,
            "long": "rotation period",
            "full_ascii": "rotation_period"
        },
        "type": "vupnu"
        },
        "logg": {
        "definition": "logarithm of the gravitational acceleration experienced at the surface of an astronomical object",
        "unit": "PureNumber",
        "remark": "when taking the log of the surface gravity, the latter should be in cm per square second",
        "wiki_data_rdf": "Q1758384",
        "label": {
            "short": "<math><mrow><mi>log</mi><mo> </mo><mi>g</mi></mrow></math>",
            "long": "surface gravity (logarthmic value)",
            "full_ascii": "log_g"
        },
        "type": "vupnu"
        },
        "t_calc": {
        "definition": "body temperature calculated by authors based on a model",
        "unit": [
            {
            "name": "kelvin",
            "symbol": "K",
            "wiki_data_rdf": "Q11579"
            }
        ],
        "wiki_data_rdf": "Q11466",
        "label": {
            "short": "<math><msub><mi>T</mi><mi>calc.</mi></msub></math>",
            "long": "calculated temperature",
            "full_ascii": "t_calc"
        },
        "type": "vupnu"
        },
        "t_meas": {
        "definition": "body temperature as measured by authors",
        "unit": [
            {
            "name": "kelvin",
            "symbol": "K",
            "wiki_data_rdf": "Q11579"
            }
        ],
        "wiki_data_rdf": "Q11466",
        "label": {
            "short": "<math><msub><mi>T</mi><mi>meas.</mi></msub></math>",
            "long": "measured temperature",
            "full_ascii": "t_meas"
        },
        "type": "vupnu"
        },
        "spectral_type": {
        "definition": "spectral class of an astronomical object",
        "wiki_data_rdf": "Q179600",
        "label": {
            "short": "sp. type",
            "long": "spectral type",
            "full_ascii": "spectral_type"
        },
        "type": "str"
        },
        "metallicity": {
        "definition": "decimal logarithm of the massive elements (metals) to hydrogen ratio in solar units  (i.e. Log [(metals/H)star/(metals/H)Sun] )",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q217030",
        "label": {
            "short": null,
            "long": "metallicity",
            "full_ascii": "metallicity"
        },
        "type": "vupnu"
        },
        "teff": {
        "definition": "it is the temperature of a black body that would emit the same total amount of electromagnetic radiation as the considered body",
        "unit": [
            {
            "name": "kelvin",
            "symbol": "K",
            "wiki_data_rdf": "Q11579"
            }
        ],
        "wiki_data_rdf": "Q6879",
        "label": {
            "short": "<math><msub><mi>T</mi><mi>eff.</mi></msub></math>",
            "long": "effective temperature",
            "full_ascii": "teff"
        },
        "type": "vupnu"
        },
        "magnetic_field": {
        "definition": "detected stellar magnetic field",
        "wiki_data_rdf": "Q6449",
        "label": {
            "short": "mag. field",
            "long": "magnetic field detected",
            "full_ascii": "magnetic_field"
        },
        "type": "bool"
        },
        "detected_disc": {
        "definition": "detected circumstellar disc",
        "wiki_data_rdf": "Q3235978",
        "label": {
            "short": null,
            "long": "disc detected",
            "full_ascii": "detected_disc"
        },
        "type": "bool"
        },
        "age": {
        "definition": "age of the object",
        "unit": [
            {
            "name": "gigayear",
            "symbol": "Gyr",
            "wiki_data_rdf": "Q896543"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": null,
            "long": "age",
            "full_ascii": "age"
        },
        "type": "vupnu"
        }
    },
    "specific_planet_parameter": {
        "label": {
        "short": "sp.planet param.",
        "long": "specific planet parameters",
        "full_ascii": "specific_planet_parameter"
        },
        "detection_method": {
        "definition": "Methods which have detected the exoplanet",
        "methods": [
            {
            "RV": {
                "definition": "Radial velocity method",
                "wiki_data_rdf": "Q2273386"
            }
            },
            {
            "timing": null
            },
            {
            "microlensing": {
                "definition": "Microlensing method",
                "wiki_data_rdf": "Q56048480"
            }
            },
            {
            "imaging": null
            },
            {
            "primary transit": null
            },
            {
            "secondary transit": null
            },
            {
            "astrometry": {
                "wiki_data_rdf": "Q181505"
            }
            },
            {
            "TTV": {
                "definition": "Transit timing variation method",
                "wiki_data_rdf": "Q2945337"
            }
            },
            {
            "disk kinematics": {
                "definition": "Detection of a Keplerian perturbation due to a planet in a gas disk"
            }
            },
            {
            "other": null
            }
        ],
        "label": {
            "short": "detect. meth.",
            "long": "detection method",
            "full_ascii": "detection_method"
        },
        "type": "str",
        "oneof": [
            "RV",
            "timing",
            "microlensing",
            "imaging",
            "primary transit",
            "secondary transit",
            "astrometry",
            "TTV",
            "disk kinematics",
            "other"
        ]
        },
        "discovery_method": {
        "definition": "Methods to discover the exoplanet",
        "methods": [
            {
            "RV": {
                "definition": "Radial velocity method",
                "wiki_data_rdf": "Q2273386"
            }
            },
            {
            "timing": null
            },
            {
            "microlensing": {
                "definition": "Microlensing method",
                "wiki_data_rdf": "Q56048480"
            }
            },
            {
            "imaging": null
            },
            {
            "primary transit": null
            },
            {
            "secondary transit": null
            },
            {
            "astrometry": {
                "wiki_data_rdf": "Q181505"
            }
            },
            {
            "TTV": {
                "definition": "Transit timing variation method",
                "wiki_data_rdf": "Q2945337"
            }
            },
            {
            "disk kinematics": {
                "definition": "Detection of a Keplerian perturbation due to a planet in a gas disk"
            }
            },
            {
            "other": null
            }
        ],
        "wiki_data_rdf": "Q591022",
        "label": {
            "short": null,
            "long": "discovery method",
            "full_ascii": "discovery_method"
        },
        "type": "str",
        "oneof": [
            "RV",
            "timing",
            "microlensing",
            "imaging",
            "primary transit",
            "secondary transit",
            "astrometry",
            "TTV",
            "disk kinematics",
            "other"
        ]
        },
        "t_peri": {
        "definition": "Time of passage at the periapse for eccentric orbits",
        "unit": [
            {
            "name": "Julian day",
            "symbol": "JD",
            "wiki_data_rdf": "Q14267"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": null,
            "long": "<math><msub><mi>T</mi><mi>peri</mi></msub></math>",
            "full_ascii": "t_peri"
        },
        "type": "vupnu"
        },
        "t_conj": {
        "definition": "Time of the star-planet upper conjunction when using the RV method",
        "unit": [
            {
            "name": "Julian day",
            "symbol": "JD",
            "wiki_data_rdf": "Q14267"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": null,
            "long": "<math><msub><mi>T</mi><mi>conj.</mi></msub></math>",
            "full_ascii": "t_conj"
        },
        "type": "vupnu"
        },
        "t_0": {
        "definition": "Time of passage at the center of the transit light curve for the primary transit",
        "unit": [
            {
            "name": "Julian day",
            "symbol": "JD",
            "wiki_data_rdf": "Q14267"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><msub><mi>T</mi><mi>0</mi></msub></math>",
            "long": "<math><msub><mi>T</mi><mrow><mi>0</mi><mrow><mo>(</mo><ms>primary</ms><mo>)</mo></mrow></mrow></msub></math>",
            "full_ascii": "t0"
        },
        "type": "vupnu"
        },
        "t0_sec": {
        "definition": "Time of passage at the center of the transit light curve for the secondary transit",
        "unit": [
            {
            "name": "Julian day",
            "symbol": "JD",
            "wiki_data_rdf": "Q14267"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><msub><mi>T</mi><mrow><mi>0</mi></mrow></msub><ms>sec</ms></math>",
            "long": "<math><msub><mi>T</mi><mrow><mi>0</mi><mrow><mo>(</mo><ms>secondary</ms><mo>)</mo></mrow></mrow></msub></math>",
            "full_ascii": "t0_sec"
        },
        "type": "vupnu"
        },
        "lambda_ang": {
        "definition": "sky-projected angle between the planetary orbital spin and the stellar rotational spin (Rossiter-McLaughlin anomaly)",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><mi>&lambda;</mi></math>",
            "long": "lambda angle",
            "full_ascii": "lambda_ang"
        },
        "type": "vupnu"
        },
        "impact_param": {
        "definition": "Minimum distance of the planet to the stellar center for transiting planets with respect to the stellar radius",
        "unit": "PureNumber",
        "remark": "% of the stellar radius, symbol : %",
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><mi>b</mi></math>",
            "long": "impact parameter",
            "full_ascii": "impact_param"
        },
        "type": "vupnu"
        },
        "tvr": {
        "definition": "Time of zero increasing radial velocity (i.e. when the planet moves towards the observer) for circular orbits",
        "unit": [
            {
            "name": "Julian day",
            "symbol": "JD",
            "wiki_data_rdf": "Q14267"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": null,
            "long": "TVR",
            "full_ascii": "tvr"
        },
        "type": "vupnu"
        },
        "k": {
        "definition": "semi-amplitude of the radial velocity curve",
        "unit": [
            {
            "name": "meter per second",
            "symbol": "m/s",
            "wiki_data_rdf": "Q182429"
            },
            {
            "name": "astronomical unit per year",
            "symbol": "au/yr",
            "wiki_data_rdf": "Q60742631"
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "<math><mi>K</mi></math>",
            "long": "K",
            "full_ascii": "k"
        },
        "type": "vupnu"
        },
        "mass_meas_method": {
        "definition": "Method of measurement of the planet mass",
        "methods": [
            {
            "RV": {
                "wiki_data_rdf": "Q2273386"
            }
            },
            {
            "microlensing": {
                "wiki_data_rdf": "Q56048480"
            }
            },
            {
            "timing": null
            },
            {
            "controversial": null
            },
            {
            "astrometry": {
                "wiki_data_rdf": "Q181505"
            }
            },
            {
            "ttv": {
                "wiki_data_rdf": "Q2945337"
            }
            },
            {
            "spectrum": null
            },
            {
            "theoretical": {
                "definition": "Mass determination derived from a theoretical model"
            }
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "mass meas. meth.",
            "long": "mass measure method",
            "full_ascii": "mass_meas_method"
        },
        "type": "str",
        "oneof": [
            "RV",
            "microlensing",
            "timing",
            "controversial",
            "astrometry",
            "ttv",
            "spectrum",
            "theoretical"
        ]
        },
        "rad_meas_method": {
        "definition": "Method of measurement of the planet radius",
        "methods": [
            {
            "primary transit": {
                "wiki_data_rdf": "Q6888"
            }
            },
            {
            "theoretical": {
                "definition": "Radius determination derived from a theoretical model"
            }
            },
            {
            "flux": {
                "wiki_data_rdf": "Q177831"
            }
            }
        ],
        "wiki_data_rdf": null,
        "label": {
            "short": "rad meas. meth.",
            "long": "radius measure method",
            "full_ascii": "rad_meas_method"
        },
        "type": "str",
        "oneof": [
            "primary transit",
            "theoretical",
            "flux"
        ]
        }
    },
    "atmospheric": {
        "label": {
        "short": "atmo.",
        "long": "atmospheric",
        "full_ascii": "atmospheric"
        },
        "hot_pt": {
        "definition": "Longitude of the planet hottest point",
        "unit": [
            {
            "name": "degree",
            "symbol": "deg",
            "wiki_data_rdf": "Q28390"
            },
            {
            "name": "hms",
            "symbol": "hh:mm:ss",
            "wiki_data_rdf": "Q13442"
            },
            {
            "name": "dms",
            "symbol": "dd:mm:ss",
            "wiki_data_rdf": "Q76287"
            }
        ],
        "remark": "counted from the substellar point",
        "wiki_data_rdf": null,
        "label": {
            "short": "hot. point",
            "long": "longitude of hottest point",
            "full_ascii": "hot_pt"
        },
        "type": "vupnu"
        },
        "albedo": {
        "definition": "Geometric Albedo (ratio of reflected radiation to incident radiation)",
        "unit": "PureNumber",
        "wiki_data_rdf": "Q2832068",
        "label": {
            "short": "geo. albedo",
            "long": "geometric albedo",
            "full_ascii": "albedo"
        },
        "type": "vupnu"
        },
        "temp": {
        "label": {
            "short": "temp.",
            "long": "temperature",
            "full_ascii": "temp"
        },
        "temp_type": {
            "definition": "type of Temperature measurement",
            "wiki_data_rdf": null,
            "label": {
            "short": "temp. type",
            "long": "temperature type",
            "full_ascii": "temp_type"
            },
            "type": "str",
            "oneof": [
            "disk_average",
            "day_average",
            "night_average",
            "spatial_variations",
            "vertical_profile",
            "3D_map",
            "hottest_point_longitude"
            ]
        },
        "temp_note": {
            "definition": "Note on Temperature measurement",
            "label": {
            "short": "temp. note",
            "long": "temperature note",
            "full_ascii": "temp_note"
            },
            "type": "str"
        },
        "temp_source": {
            "definition": "Source of the temperature data",
            "label": {
            "short": "temp. src.",
            "long": "temperature source",
            "full_ascii": "temp_source"
            },
            "type": "str",
            "oneof": [
            "measurement",
            "modelling"
            ]
        },
        "temp_result": {
            "definition": "Temperature value",
            "label": {
            "short": "temp. res.",
            "long": "temperature result",
            "full_ascii": "temp_result"
            },
            "type": "str"
        },
        "temp_fig": {
            "definition": "Link to a figure showing the temperature measurement",
            "label": {
            "short": "temp. fig.",
            "long": "temperature figure",
            "full_ascii": "temp_fig"
            },
            "type": "str"
        }
        },
        "molecule": {
        "label": {
            "short": null,
            "long": "molecule",
            "full_ascii": "molecule"
        },
        "molecule_type": {
            "definition": "type of molecule measurement",
            "wiki_data_rdf": null,
            "type": "str"
        },
        "molecule_name": {
            "definition": "Name of Molecule",
            "type": "list"
        },
        "molecule_note": {
            "definition": "Note on Molecule measurement",
            "type": "str"
        },
        "molecule_result": {
            "definition": "Molecule mixing ratio value",
            "type": "str"
        },
        "molecule_source": {
            "definition": "Source of the molecule data",
            "type": "str",
            "oneof": [
            "measurement",
            "modelling"
            ]
        },
        "molecule_fig": {
            "definition": "Link to a figure showing the molecule observation",
            "type": "str"
        }
        }
    }
    }
}'),
('{
    "references": {
    "required": true,
    "publication": {
        "required": true,
        "label": {
        "short": "pub.",
        "long": "publication",
        "full_ascii": "publication"
        },
        "title": {
        "definition": "publication title",
        "label": {
            "short": null,
            "long": "title",
            "full_ascii": "title"
        },
        "type": "str"
        },
        "author": {
        "definition": "publication authors",
        "label": {
            "short": null,
            "long": "author",
            "full_ascii": "author"
        },
        "type": "str"
        },
        "date": {
        "definition": "publication date",
        "label": {
            "short": null,
            "long": "date",
            "full_ascii": "date"
        },
        "type": "str"
        },
        "publication_type": {
        "definition": "publication type",
        "label": {
            "short": null,
            "long": "type",
            "full_ascii": "type"
        },
        "type": "str",
        "oneof": [
            "book",
            "thesis",
            "refereed article",
            "proceeding",
            "unknown",
            "report"
        ]
        },
        "journal_name": {
        "definition": "publication journal name",
        "label": {
            "short": null,
            "long": "journal name",
            "full_ascii": "journal_name"
        },
        "type": "str"
        },
        "journal_volume": {
        "definition": "publication journal volume",
        "label": {
            "short": null,
            "long": "journal volume",
            "full_ascii": "journal_volume"
        },
        "type": "str"
        },
        "journal_page": {
        "definition": "publication journal page",
        "label": {
            "short": null,
            "long": "journal page",
            "full_ascii": "journal_page"
        },
        "type": "str"
        },
        "bib_code": {
        "definition": "publication bibcode",
        "label": {
            "short": null,
            "long": "bib code",
            "full_ascii": "bib_code"
        },
        "type": "str"
        },
        "keyword": {
        "definition": "publication keyword",
        "label": {
            "short": null,
            "long": "keyword",
            "full_ascii": "keyword"
        },
        "type": "list"
        },
        "ref": {
        "definition": "publication references",
        "label": {
            "short": "ref.",
            "long": "references",
            "full_ascii": "ref"
        },
        "type": "str"
        },
        "doi": {
        "definition": "publication DOI",
        "label": {
            "short": "DOI",
            "long": "digital object identifier",
            "full_ascii": "doi"
        },
        "type": "str",
        "required": true,
        "nullable": true
        },
        "url": {
        "definition": "publication url",
        "label": {
            "short": null,
            "long": "URL",
            "full_ascii": "url"
        },
        "type": "str",
        "required": true,
        "nullable": true
        },
        "publication_status": {
        "definition": "status of the reveal s object publication, must be in this list : R — Published in a refereed paper, S — Submitted to a professional journal, C — Announced on a professional conference, W — Announced on a website",
        "label": {
            "short": "pub. status",
            "long": "publication status",
            "full_ascii": "publication_status"
        },
        "type": "str",
        "oneof": [
            "R",
            "S",
            "C",
            "W"
        ]
        }
    },
    "links": {
        "definition": "all useful link",
        "label": {
        "short": null,
        "long": "links",
        "full_ascii": "links"
        },
        "type": "list"
    },
    "url_simbad": {
        "definition": "url corresponding to the object on simba",
        "label": {
        "short": null,
        "long": "url simbad",
        "full_ascii": "url_simbad"
        },
        "type": "str"
    }
    }
}'),
('{
    "relationship": {
    "required": true,
    "nullable": true,
    "gravitational_system": {
        "definition": "Gravitational system, in this part you can fill in all the gravitational relations of the objects described in the file in list with the format described below cf. [Document name] for examples",
        "label": {
        "short": "grav. sys.",
        "long": "gravitational system",
        "full_ascii": "gravitational_system"
        },
        "keysrules": "^gs[0-9]+$",
        "valuesrules": "list"
    }
    }
}');


-- ID REFERENCE
-- exo_type = 1
-- identification = 2
-- internal = 3
-- parameters = 4
-- reference = 5
-- relationship = 6

-- INSERT data into exodict table
INSERT INTO exodam.exodict (refereed) VALUES (True); -- id = 1

-- INSERT data into exodict2data table
INSERT INTO exodam.exodict2data (id_exodict, name, id_data)
VALUES
(1, 'exo_type', 1),
(1, 'identification', 2),
(1, 'internal', 3),
(1, 'parameters', 4),
(1, 'reference', 5),
(1, 'relationship', 6);


-- INSERT data into exodict_editorial_data table
INSERT INTO exodam.exodict_editorial_data (data) VALUES
('{
    "news": {
        "label": {
            "short": null,
            "long": "news",
            "full_ascii": "news"
        },
        "title": {
        "definition": "Title of the news.",
        "wiki_data_rdf": null,
        "label": {
            "short": null,
            "long": "title",
            "full_ascii": "title"
        },
        "required": true,
        "type": "str"
        },
        "body": {
        "definition": "Body of the news.",
        "wiki_data_rdf": null,
        "label": {
            "short": "body",
            "long": "news body",
            "full_ascii": "news_body"
        },
        "required": true,
        "type": "str"
        },
        "image": {
        "definition": "Image of the news.",
        "wiki_data_rdf": null,
        "label": {
            "short": "img.",
            "long": "image",
            "full_ascii": "image"
        },
        "type": "str",
        "nullable": true
        },
        "links": {
        "definition": "Useful links.",
        "wiki_data_rdf": null,
        "label": {
            "short": "links",
            "long": "useful links",
            "full_ascii": "useful_links"
        },
        "type": "list"
        },
        "news_type": {
        "definition": "Type of the news.",
        "wiki_data_rdf": null,
        "label": {
            "short": "type",
            "long": "news type",
            "full_ascii": "news_type"
        },
        "required": true,
        "type": "str",
        "oneof": [
            "scientific",
            "technical",
            "announcement"
        ]
        },
        "web_status": {
        "definition": "Web status, internal parameters.",
        "wiki_data_rdf": null,
        "label": {
            "short": "web st.",
            "long": "web status",
            "full_ascii": "web_status"
        },
        "required": true,
        "type": "str",
        "oneof": [
            "active",
            "hidden"
        ]
        }
    }
}');

-- ID REFERENCE
-- news = 1


-- INSERT data into exodict_editorial table
INSERT INTO exodam.exodict_editorial (refereed) VALUES (True); -- id = 1

-- INSERT data into exodict_editorial2data table
INSERT INTO exodam.exodict_editorial2data (id_exodict, name, id_data)
VALUES (1, 'news', 1);

-- INSERT data INTO exodam_editorial_skel
INSERT INTO exodam.exodam_editorial_skel (refereed, data) VALUES
(
    True,
    '{
        "news": {
            "title": null,
            "body": null,
            "image": null,
            "links": null,
            "news_type": null,
            "web_status": null
        }
    }'
);

-- INSERT data into exodam editorial known keys table
INSERT INTO exodam.exodam_editorial_known_keys (refereed, data) VALUES
(
    True,
    '{
        "known_keys": [
            "label",
            "short",
            "long",
            "full_ascii",
            "news",
            "title",
            "body",
            "image",
            "links",
            "news_type",
            "web_status",
            "wiki_data_rdf"
        ]
    }'
);

-- INSERT data into exodam_editorial_cerberus_data table
INSERT INTO exodam.exodam_editorial_cerberus_data (data) VALUES
('{
    "news": {
    "type": "list",
    "schema": {
        "type": "dict",
        "schema": {
            "title": {
                "type": "string",
                "required": true
            },
            "body": {
                "type": "string",
                "required": true
            },
            "image": {
                "nullable": true,
                "type": "string"
            },
            "links": {
                "type": "list",
                "schema": {
                    "type": "string"
                }
            },
            "news_type": {
                "type": "string",
                "required": true,
                "oneof": [
                    {
                    "type": "string",
                    "regex": "scientific"
                    },
                    {
                    "type": "string",
                    "regex": "technical"
                    },
                    {
                    "type": "string",
                    "regex": "announcement"
                    }
                ]
            },
            "web_status": {
                "type": "string",
                "required": true,
                "oneof": [
                    {
                    "type": "string",
                    "regex": "active"
                    },
                    {
                    "type": "string",
                    "regex": "hidden"
                    }
                ]
            }
        }
    }
}
}');

-- ID REFERENCE
-- base -> news = 1

-- INSERT data into exodam_editorial_cerberus table
INSERT INTO exodam.exodam_editorial_cerberus (refereed) VALUES (True); -- id = 1

-- INSERT data into exodam_editorial_cerberus_data table
INSERT INTO exodam.exodam_editorial_cerberus2data (id_cerberus, name, cerberus_type, id_cerberus_data)
VALUES
(1, 'news', 'type__base', 1);
