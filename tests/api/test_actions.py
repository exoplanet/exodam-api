# Test imports
from assertpy import assert_that, soft_assertions

# Third party imports
from fastapi import status


def test_api__actions(client):
    response = client.get("/exodam/api/actions")
    expected_json = {
        "details": "All different actions/services available via the API. "
        "These services do not relate directly to a resource. They don't change "
        "the state of the server, but have side effects.",
        "data": {
            "avaible_actions": {
                "diff": "Does not change the server state, but generate diff between "
                "two yaml file as a side effect",
                "check": "Does not change the server state, but generates a check of a "
                "yaml file as a side effect.",
                "scientific-convert-exodam": "Does not change the server state, but "
                "convert a scientific yaml file in an exodam file as a side effect.",
                "editorial-convert-exodam": "Does not change the server state, but "
                "convert a editorial yaml file in an exodam file as a side effect.",
            }
        },
    }

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(response.json()).is_equal_to(expected_json)
