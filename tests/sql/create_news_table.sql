-- CREATE news table
CREATE TABLE exodam.editorial_exodam_news(
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    exodict_version text NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
