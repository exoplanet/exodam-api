# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Third party imports
from fastapi import status

# Local imports
from .conftest import check_exp_msg_in_warning_msg


@pytest.mark.parametrize(
    "file, exp_details, is_valid",
    [
        param("all_good.yaml", "Is valid file", True, id="yaml valid file"),
        param("all_good_yml.yml", "Is valid file", True, id="yml valid file"),
        param(
            "identity_exotype_regex_error.yaml",
            "Is not a valid file",
            False,
            id="invalid: identity exotype regex error",
        ),
        param(
            "missing_identity.yaml",
            "Is not a valid file",
            False,
            id="invalid: missing indentity",
        ),
    ],
)
@pytest.mark.parametrize(
    "permissive, exp_warning",
    [
        param(
            False,
            "Struct and global information check",
            id="permissive: False",
        ),
        param(
            True,
            "Permissive",
            id="permissive: True",
        ),
    ],
)
@pytest.mark.parametrize(
    "doi",
    [
        param(False, id="with doi: False"),
        param(True, id="with doi: True"),
    ],
)
def test_api__check_ok(
    testing_files_folder_path,
    client,
    file,
    exp_details,
    is_valid,
    permissive,
    doi,
    exp_warning,
):
    with open(f"{testing_files_folder_path}/check/{file}", "rb") as open_file:
        post_file = {"file_to_check": open_file}
        options = {"permissive": permissive, "with_doi": doi}
        response = client.post(
            "/exodam/api/actions/check/",
            files=post_file,
            data=options,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]["file"]).is_equal_to(file)
        assert_that(json_response["data"]["options"]).is_equal_to(options)
        assert_that(json_response["data"]["check"]).is_equal_to(is_valid)
        assert_that(
            check_exp_msg_in_warning_msg(
                exp_warning, json_response["data"]["warning_msg"]
            )
        ).is_equal_to(True)


@pytest.mark.parametrize(
    "file, exp_details, is_valid, permissive, exp_warning",
    [
        param(
            "permissive.yaml",
            "Is not a valid file",
            False,
            False,
            "Struct and global information check",
            id="not permissive =>` not valid",
        ),
        param(
            "permissive.yaml",
            "Is valid file",
            True,
            True,
            "Permissive",
            id="permissive => valid",
        ),
    ],
)
@pytest.mark.parametrize(
    "doi",
    [
        param(False, id="with doi: False"),
        param(True, id="with doi: True"),
    ],
)
def test_api__check_ok_valid_only_in_permissive(
    testing_files_folder_path,
    client,
    file,
    exp_details,
    is_valid,
    permissive,
    exp_warning,
    doi,
):
    with open(f"{testing_files_folder_path}/check/{file}", "rb") as open_file:
        post_file = {"file_to_check": open_file}
        options = {"permissive": permissive, "with_doi": doi}
        response = client.post(
            "/exodam/api/actions/check/",
            files=post_file,
            data=options,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]["file"]).is_equal_to(file)
        assert_that(json_response["data"]["options"]).is_equal_to(options)
        assert_that(json_response["data"]["check"]).is_equal_to(is_valid)
        assert_that(
            check_exp_msg_in_warning_msg(
                exp_warning, json_response["data"]["warning_msg"]
            )
        ).is_equal_to(True)


@pytest.mark.parametrize(
    "file, exp_details, is_valid, doi",
    [
        param(
            "valid_without_doi.yml",
            "Is valid file",
            True,
            False,
            id="without doi =>` valid",
        ),
        param(
            "valid_without_doi.yml",
            "Is not a valid file",
            False,
            True,
            id="with doi => not valid",
        ),
    ],
)
def test_api__check_ok_valid_only_without_doi(
    testing_files_folder_path, client, file, exp_details, is_valid, doi
):
    with open(f"{testing_files_folder_path}/check/{file}", "rb") as open_file:
        post_file = {"file_to_check": open_file}
        options = {"permissive": True, "with_doi": doi}
        response = client.post(
            "/exodam/api/actions/check/",
            files=post_file,
            data=options,
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]["file"]).is_equal_to(file)
        assert_that(json_response["data"]["options"]).is_equal_to(options)
        assert_that(json_response["data"]["check"]).is_equal_to(is_valid)
        assert_that(
            check_exp_msg_in_warning_msg(
                "Permissive", json_response["data"]["warning_msg"]
            )
        ).is_equal_to(True)


@pytest.mark.parametrize(
    "file, exp_warnings",
    [
        param(
            "README.md",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. for file -> md",
        ),
        param(
            "invalid.yaml",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file invalid.yaml is empty.",
            ],
            id="file is empty",
        ),
    ],
)
def test_api__check_unsupported_media_type(
    testing_files_folder_path,
    client,
    file,
    exp_warnings,
):
    with open(f"{testing_files_folder_path}/check/{file}", "rb") as open_file:
        post_file = {"file_to_check": open_file}
        response = client.post("/exodam/api/actions/check/", files=post_file)

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
        assert_that(json_response["details"]).is_equal_to("Unsupported Media Type")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)
