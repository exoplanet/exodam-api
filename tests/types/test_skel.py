# Test imports
import pytest
from assertpy import assert_that
from pytest import param

# First party imports
from exodam_api import SkelType as sT


@pytest.mark.parametrize(
    "type, expected_value",
    [
        param(sT.CSV, "csv", id="csv type"),
        param(sT.YAML, "yaml", id="yaml type"),
        param(sT.YML, "yml", id="yml type"),
    ],
)
def test_skel_type(type, expected_value):
    assert_that(type.value).is_equal_to(expected_value)


def test_skel_type_as_str():
    assert_that(sT.as_str_lst()).is_equal_to(["yaml", "yml", "csv"])
