# Test imports
import pytest

# Standard imports
from pathlib import Path

# Third party imports
from fastapi.testclient import TestClient

# First party imports
from main import app

CLIENT = TestClient(app)


def check_exp_msg_in_warning_msg(exp_msg: str, warning_msg: list[str]) -> bool:
    """
    Check if an expected message is in on one element of warning messages.

    Args:
        exp_msg: expected message.
        warning_msg: Warning messages.

    Returns:
        True if exp_msg is in on one element of warning_msg, False otherwise.

    Examples:
        >>> war_msg = ['toto is toto', 'tutu is titi']

        >>> check_exp_msg_in_warning_msg('tutu', war_msg)
        True

        >>> check_exp_msg_in_warning_msg('tata', war_msg)
        False
    """
    for msg in warning_msg:
        if exp_msg in msg:
            return True

    return False


@pytest.fixture(scope="session")
def client():
    return CLIENT


@pytest.fixture(scope="session")
def skel_ok_expected_response():
    return {
        "identity": {
            "alternate_names": None,
            "doi": None,
            "exo_type": None,
            "name": None,
        },
        "internal": {
            "display_status": None,
            "embargo_end_date": None,
            "object_status": None,
        },
        "parameters": {
            "atmospheric": {
                "albedo": None,
                "hot_pt": None,
                "molecule": None,
                "temp": {
                    "temp_fig": None,
                    "temp_note": None,
                    "temp_result": None,
                    "temp_source": None,
                    "temp_type": None,
                },
            },
            "magnitude": {
                "magnitude_h": None,
                "magnitude_i": None,
                "magnitude_j": None,
                "magnitude_k": None,
                "magnitude_v": None,
            },
            "orbital": {
                "angular_dis": None,
                "eccentricity": None,
                "inclination": None,
                "omega": None,
                "period": None,
                "semi_major_axis": None,
            },
            "physical": {
                "age": None,
                "density": None,
                "detected_disc": None,
                "logg": None,
                "magnetic_field": None,
                "mass": None,
                "metallicity": None,
                "radius": None,
                "rotation_period": None,
                "spectral_type": None,
                "t_calc": None,
                "t_meas": None,
                "teff": None,
            },
            "position": {
                "dec": None,
                "distance": None,
                "pro_motion": None,
                "pro_motion_dec": None,
                "pro_motion_ra": None,
                "ra": None,
                "radial_velocity": None,
            },
            "specific_planet_parameter": {
                "detection_method": None,
                "discovery_method": None,
                "impact_param": None,
                "k": None,
                "lambda_ang": None,
                "mass_meas_method": None,
                "rad_meas_method": None,
                "t0_sec": None,
                "t_0": None,
                "t_conj": None,
                "t_peri": None,
                "tvr": None,
            },
        },
        "references": {
            "links": None,
            "publication": {
                "author": None,
                "bib_code": None,
                "date": None,
                "doi": None,
                "journal_name": None,
                "journal_page": None,
                "journal_volume": None,
                "keyword": None,
                "publication_status": None,
                "publication_type": None,
                "ref": None,
                "title": None,
                "url": None,
            },
            "url_simbad": None,
        },
        "relationship": {"gravitational_system": None},
    }
