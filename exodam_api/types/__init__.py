"""Useful types for the API."""

# Local imports
from .exodam_response import (  # noqa: F401
    ExodamResponseError,
    ExodamResponseVersion,
    ExodamResponseWithData,
    ExodamResponseWithoutData,
)
from .skel_type import SkelType  # noqa:F401
