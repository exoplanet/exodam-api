"""Useful functions for the API."""

# Standard imports
from pathlib import Path


def have_good_suffix(file: Path) -> bool:
    """
    Check if a path have a `.yaml` or `.yml` suffix.

    Args:
        file: File to check.

    Returns:
        True if file is a `yaml` or `yml` file, False otherwise.

    Examples:
        >>> have_good_suffix(Path("toto.exodam.json"))
        True

        >>> have_good_suffix(Path("toto.yml"))
        True

        >>> have_good_suffix(Path("toto.yaml"))
        True

        >>> have_good_suffix(Path("toto.json"))
        False

        >>> have_good_suffix(Path("toto.md"))
        False
    """
    if file.suffix == ".json":
        return file.suffixes == [".exodam", ".json"]
    return file.suffix in [".yaml", ".yml"]
