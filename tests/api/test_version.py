# Test imports
from assertpy import assert_that, soft_assertions

# Third party imports
from fastapi import status

# First party imports
from exodam_api import __version__


def test_api__version(client):
    response = client.get("/exodam/api")
    expected_json = {
        "details": "EXODAM-API is an API to help in the creation and manipulation of "
        "exoplanets and other exo-objects (exomoons, discs, comets...) "
        "description files.",
        "version": __version__,
    }

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(response.json()).is_equal_to(expected_json)
