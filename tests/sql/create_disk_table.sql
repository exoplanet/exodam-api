-- CREATE disk table
CREATE TABLE exodam.disk(
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    exodict_version text NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
