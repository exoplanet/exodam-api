# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Standard imports
from contextlib import contextmanager
from io import TextIOWrapper
from pathlib import Path
from typing import Generator

# Exodam
from exodam_utils import load_yaml

# Third party imports
from dotmap import DotMap
from fastapi import status
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_api import get_conn

# Local imports
from ...conftest import TESTING_FILE_FOLDER_PATH

TESTING_ADD_EXODICT_FOLDER_PATH = TESTING_FILE_FOLDER_PATH / "add/exodict/scientific"


def get_ressources_info_from_db():
    conn = get_conn()

    last_cerberus_id = (
        SQLEnumerable(conn, "exodam.exodam_cerberus")
        .select(lambda x: x.id)
        .where(lambda x: x.refereed == True)
        .execute()[0]
        .id
    )

    result = DotMap(
        {
            "last_cerberus_id": last_cerberus_id,
            "last_cerberus2data_id": SQLEnumerable(conn, "exodam.exodam_cerberus2data")
            .select(lambda x: x.id)
            .where(lambda x: x.id_cerberus == last_cerberus_id)
            .execute(),
            "count_cerberus_data": SQLEnumerable(conn, "exodam.exodam_cerberus_data")
            .select(lambda x: x.id)
            .count()
            .execute()[0]
            .count,
            "last_exodam_types_id": SQLEnumerable(conn, "exodam.exodam_types")
            .select(lambda x: x.id)
            .where(lambda x: x.refereed == True)
            .execute()[0]
            .id,
            "last_known_keys_id": SQLEnumerable(conn, "exodam.exodam_known_keys")
            .select(lambda x: x.id)
            .where(lambda x: x.refereed == True)
            .execute()[0]
            .id,
            "last_vupnu_keys_id": SQLEnumerable(conn, "exodam.exodam_vupnu_keys")
            .select(lambda x: x.id)
            .where(lambda x: x.refereed == True)
            .execute()[0]
            .id,
            "last_yml_skel_id": SQLEnumerable(conn, "exodam.exodam_skel")
            .select(lambda x: x.id)
            .where(lambda x: x.refereed == True)
            .execute()[0]
            .id,
        }
    )

    conn.close()

    return result


@contextmanager
def _open_if_not_none_all_files(
    files_to_open: list[Path | None],
) -> Generator[dict[str, TextIOWrapper], None, None]:
    """
    Open file if is not None.

    Args:
        files_to_open: List of files to open or None for no file.

    Returns:
        Opened files.
    """
    res = {}
    for file_to_open in files_to_open:
        if file_to_open is not None:
            res[file_to_open.stem] = open(file_to_open, "rb")

    yield res

    for opened_file in res.values():
        opened_file.close()


@pytest.mark.parametrize(
    "exo_type_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "exo_type.yml", id="exo_type is not None"
        ),
        param(None, id="exo_type is None"),
    ],
)
@pytest.mark.parametrize(
    "identification_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "identification.yml",
            id="identification is not None",
        ),
        param(None, id="identification is None"),
    ],
)
@pytest.mark.parametrize(
    "internal_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "internal.yml", id="internal is not None"
        ),
        param(None, id="internal is None"),
    ],
)
@pytest.mark.parametrize(
    "parameters_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "parameters.yml",
            id="parameters is not None",
        ),
        param(None, id="parameters is None"),
    ],
)
@pytest.mark.parametrize(
    "reference_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "reference.yml",
            id="reference is not None",
        ),
        param(None, id="reference is None"),
    ],
)
@pytest.mark.parametrize(
    "relationship_path_or_none",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "relationship.yml",
            id="relationship is not None",
        ),
        param(None, id="relationship is None"),
    ],
)
@pytest.mark.slow
def test_api__add_exodict_ok(
    client,
    exo_type_path_or_none,
    identification_path_or_none,
    internal_path_or_none,
    parameters_path_or_none,
    reference_path_or_none,
    relationship_path_or_none,
):
    ressources_info_before = get_ressources_info_from_db()

    given_exodict_part = [
        exo_type_path_or_none,
        identification_path_or_none,
        internal_path_or_none,
        parameters_path_or_none,
        reference_path_or_none,
        relationship_path_or_none,
    ]

    with _open_if_not_none_all_files(given_exodict_part) as opened_files:
        response = client.post(
            f"/exodam/api/exodict/",
            files=opened_files,
        )

    json_response = response.json()

    _get_file_name_or_none = lambda file_name_or_none: (
        file_name_or_none.name if file_name_or_none is not None else None
    )

    files = {
        "exo_type": _get_file_name_or_none(exo_type_path_or_none),
        "identification": _get_file_name_or_none(identification_path_or_none),
        "internal": _get_file_name_or_none(internal_path_or_none),
        "parameters": _get_file_name_or_none(parameters_path_or_none),
        "reference": _get_file_name_or_none(reference_path_or_none),
        "relationship": _get_file_name_or_none(relationship_path_or_none),
    }
    reference_files = {key: value for key, value in files.items() if value is None}
    new_files = {key: str(value) for key, value in files.items() if value is not None}

    expected_details = (
        "No new file given. We do nothing. 0 new files, 6 reference files."
        if all(file is None for file in given_exodict_part)
        else f"New exodict has been inserted without problems. {len(new_files)} new "
        f"files, {len(reference_files)} reference files."
    )

    if any(new_files.values()):
        common_gap = 1
        cerb2data_gap = 9
    else:
        common_gap = 0
        cerb2data_gap = 0

    ressources_info_after = get_ressources_info_from_db()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(expected_details)
        assert_that(json_response["data"]["reference_files"]).is_equal_to(
            reference_files
        )
        assert_that(json_response["data"]["new_files"]).is_equal_to(new_files)

        for exodict_part in given_exodict_part:
            if exodict_part is not None:
                assert_that(load_yaml(exodict_part)).is_subset_of(
                    json_response["data"]["final_exodict"]
                )

        # Ressources
        assert_that(ressources_info_after.last_cerberus_id).is_equal_to(
            ressources_info_before.last_cerberus_id + common_gap
        )

        assert_that(len(ressources_info_after.last_cerberus2data_id)).is_equal_to(
            len(ressources_info_before.last_cerberus2data_id)
        )
        for cerberus2data, before_cerberus2data in zip(
            ressources_info_after.last_cerberus2data_id,
            ressources_info_before.last_cerberus2data_id,
        ):
            assert_that(cerberus2data.id).is_equal_to(
                before_cerberus2data.id + cerb2data_gap
            )

        assert_that(ressources_info_after.count_cerberus_data).is_equal_to(
            ressources_info_before.count_cerberus_data + cerb2data_gap
        )

        assert_that(ressources_info_after.last_exodam_types_id).is_equal_to(
            ressources_info_before.last_exodam_types_id + common_gap
        )
        assert_that(ressources_info_after.last_known_keys_id).is_equal_to(
            ressources_info_before.last_known_keys_id + common_gap
        )
        assert_that(ressources_info_after.last_vupnu_keys_id).is_equal_to(
            ressources_info_before.last_vupnu_keys_id + common_gap
        )
        assert_that(ressources_info_after.last_yml_skel_id).is_equal_to(
            ressources_info_before.last_yml_skel_id + common_gap
        )


@pytest.mark.parametrize(
    "wrong_file",
    [
        param("exo_type", id="exo_type"),
        param("identification", id="identification"),
        param("internal", id="internal"),
        param("parameters", id="parameters"),
        param("reference", id="reference"),
        param("relationship", id="relationship"),
    ],
)
@pytest.mark.parametrize(
    "path_file, exp_warnings",
    [
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "empty.yml",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file " "empty.yml is empty.",
            ],
            id="empty file",
        ),
        param(
            TESTING_ADD_EXODICT_FOLDER_PATH / "README.md",
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file "
                "README.md. "
                "Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="wrong type, not yml or yaml",
        ),
    ],
)
def test_api__add_exodict_unsupported_media_type(
    client,
    wrong_file,
    path_file,
    exp_warnings,
):
    with open(path_file, "rb") as opened_file:
        response = client.post(
            f"/exodam/api/exodict/",
            files={wrong_file: opened_file},
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
        assert_that(json_response["details"]).is_equal_to("Unsupported Media Type")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)


def test_api__add_exodict_py_linq_sql_error(
    client,
    remove_and_re_add_exodict_table,
):
    # Valid file but we removed the corresponding table in the DB to cause the error
    VALID_FILE = TESTING_ADD_EXODICT_FOLDER_PATH / "relationship.yml"

    with open(VALID_FILE, "rb") as opened_relationship:
        response = client.post(
            f"/exodam/api/exodict/",
            files={"relationship": opened_relationship},
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        assert_that(json_response["details"]).is_equal_to("Internal Server Error")
        assert_that("\n".join(json_response["warnings"])).contains(
            "psycopg.errors.UndefinedTable"
        )


def test_api__add_exodict_exodam_error(client):
    # Invalid file missing a full_ascii label that is mandatory
    INVALID_FILE = TESTING_ADD_EXODICT_FOLDER_PATH / "wrong_relationship.yml"

    with open(INVALID_FILE, "rb") as opened_relationship:
        response = client.post(
            f"/exodam/api/exodict/",
            files={"relationship": opened_relationship},
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        assert_that(json_response["details"]).is_equal_to("Internal Server Error")
        assert_that("\n".join(json_response["warnings"])).contains(
            "exodict.exceptions.LabelError"
        )


# In fact it's not possible IRL to have that error since we are not calling
# get_exodict() with a name parameter (ans only that can trigger a ValueError)
def test_api__add_exodict_value_error(client, mocker):
    # Let's fake the call to the underlying function since we are just testing the API
    fake_get_exodict = mocker.patch("main.get_exodict", side_effect=ValueError)

    # Valid file but we mock the get_exodict() underlying func to cause an error
    VALID_FILE = TESTING_ADD_EXODICT_FOLDER_PATH / "relationship.yml"

    with open(VALID_FILE, "rb") as opened_relationship:
        response = client.post(
            f"/exodam/api/exodict/",
            files={"relationship": opened_relationship},
        )

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        assert_that(json_response["details"]).is_equal_to("Internal Server Error")
        assert_that("\n".join(json_response["warnings"])).contains(f"{ValueError}")
