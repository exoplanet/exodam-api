# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Third party imports
from fastapi import status

# First party imports
from exodam_api import __version__


@pytest.mark.parametrize(
    "param",
    [
        param("yaml", id="param: yaml"),
        param("yml", id="param: yml"),
    ],
)
def test_api__give_skel_ok(client, param, skel_ok_expected_response):
    response = client.get(f"/exodam/api/skel/?sk_type={param}")

    json_response = response.json()
    expected_details = "A json/yaml skeleton file to create an exodam file."
    expected_data = skel_ok_expected_response

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(expected_details)
        assert_that(json_response["data"]).is_equal_to(expected_data)


@pytest.mark.parametrize(
    "param, exp_code, exp_json",
    [
        param(
            "csv",
            status.HTTP_501_NOT_IMPLEMENTED,
            {
                "details": "Not implemented feature.",
                "warnings": [
                    "CSV skeleton is not yet implemented.",
                    "Check Readme, Changelog and issues of the exodam project for more "
                    "informations",
                ],
            },
            id="param: csv",
        ),
        param(
            "toto",
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            {
                "detail": [
                    {
                        "type": "enum",
                        "loc": ["query", "sk_type"],
                        "msg": "Input should be 'yaml','yml' or 'csv'",
                        "input": "toto",
                        "ctx": {"expected": "'yaml','yml' or 'csv'"},
                    }
                ]
            },
            id="param: other",
        ),
    ],
)
def test_api__give_skel_error(client, param, exp_code, exp_json):
    response = client.get(f"/exodam/api/skel/?sk_type={param}")

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(exp_code)
        assert_that(response.json()).is_equal_to(exp_json)
