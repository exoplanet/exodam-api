"""Connections to db functions fro the API."""

# Standard imports
from os import getenv
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq_sql import connect


# TODO: tests
# TODO : faire mieux, See: Exodam
def get_conn() -> Connection:
    """
    Get a connection form environment variables.

    Returns:
        A connection to a db.
    """
    return connect(
        user=cast(str, getenv("LOCAL_DB_USER")),
        password=cast(str, getenv("LOCAL_DB_PASSWORD")),
        host=cast(str, getenv("LOCAL_DB_HOST")),
        port=cast(str, getenv("LOCAL_DB_PORT")),
        database=cast(str, getenv("LOCAL_DB_DATABASE_NAME")),
    )
