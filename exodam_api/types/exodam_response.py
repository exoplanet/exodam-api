"""Response class for the API."""

# Standard imports
from typing import Any

# Third party imports
from pydantic import BaseModel

# TODO: add class or use an existing class for submit an exodam file


class _ExodamResponse(BaseModel):
    """Exodam response based class."""

    details: str
    "Some details of the response."


class ExodamResponseWithData(_ExodamResponse):
    """
    Exodam response with data field.

    Inherited custom attributes:
        details: Some details of the response.
    """

    data: dict[str, Any]
    "Data of the response."


class ExodamResponseWithoutData(_ExodamResponse):
    """
    Exodam response without data field.

    Inherited custom attributes:
        details: Some details of the response.
    """


class ExodamResponseError(ExodamResponseWithoutData):
    """
    Exodam response with data field.

    Inherited custom attributes:
        details: Some details of the response.
        data: Data of the response.
    """

    warnings: list[str]
    "All warning get by the request."


class ExodamResponseVersion(ExodamResponseWithoutData):
    """
    Exodam response for the version.

    Inherited custom attributes:
        details: Some details of the response.
    """

    version: str
    "Version of the API."
