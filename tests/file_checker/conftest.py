# Test imports
import pytest

# Third party imports
from fastapi import UploadFile
from yaml import safe_load as yml_load

# First party imports
from exodam_api import FileChecker


@pytest.fixture(scope="function")
def FileCheckerInstanceYAML_file(testing_files_folder_path):
    with open(testing_files_folder_path / "check/all_good.yaml", "rb") as file:
        upload_file = UploadFile(file, filename="all_good.yaml")

        yield FileChecker(upload_file)


@pytest.fixture(scope="function")
def FileCheckerInstanceJSON_file(testing_files_folder_path):
    with open(testing_files_folder_path / "check/scientific.exodam.json", "rb") as file:
        upload_file = UploadFile(file, filename="scientific.exodam.json")

        yield FileChecker(upload_file)


@pytest.fixture(scope="function")
def FileCheckerInstanceYML_file(testing_files_folder_path):
    with open(testing_files_folder_path / "check/all_good_yml.yml", "rb") as file:
        upload_file = UploadFile(file, filename="all_good_yml.yml")

        yield FileChecker(upload_file)


@pytest.fixture(scope="function")
def FileCheckerInstanceYAML_path(testing_files_folder_path):
    return FileChecker(testing_files_folder_path / "check/all_good.yaml")


@pytest.fixture(scope="function")
def FileCheckerInstanceJSON_path(testing_files_folder_path):
    return FileChecker(testing_files_folder_path / "check/scientific.exodam.json")


@pytest.fixture(scope="function")
def FileCheckerInstanceYML_path(testing_files_folder_path):
    return FileChecker(testing_files_folder_path / "check/all_good_yml.yml")


@pytest.fixture(scope="session")
def yaml_content_expected(testing_files_folder_path):
    with open(testing_files_folder_path / "check/all_good.yaml", "rb") as file:
        return yml_load(file)


@pytest.fixture(scope="session")
def json_content_expected(testing_files_folder_path):
    with open(testing_files_folder_path / "check/scientific.exodam.json", "rb") as file:
        return yml_load(file)
