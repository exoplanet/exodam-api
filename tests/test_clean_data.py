# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Third party imports
from flatten_dict import flatten, unflatten
from flatten_dict.reducers import make_reducer

# First party imports
from exodam_api import DICT_FLATTEN_DELIMITER, clean_none_data


@pytest.mark.parametrize(
    "data, expected",
    [
        param([{}], [{}], id="empty"),
        param([{"a": 1, "b": 2}], [{"a": 1, "b": 2}], id="simple without None"),
        param([{"a": 1, "b": None}], [{"a": 1}], id="simple with None"),
        param(
            [{"a": 1, "b": {"c": 2, "d": 3}}],
            [{"a": 1, "b": {"c": 2, "d": 3}}],
            id="nested without None ",
        ),
        param(
            [{"a": None, "b": {"c": 2, "d": 3}}],
            [{"b": {"c": 2, "d": 3}}],
            id="nested with None in first floor",
        ),
        param(
            [{"a": 1, "b": {"c": None, "d": 3}}],
            [{"a": 1, "b": {"d": 3}}],
            id="nested with None in nested",
        ),
    ],
)
def test_clean_none_data(data, expected):
    res = clean_none_data(data)
    with soft_assertions():
        assert_that(res).is_equal_to(expected)
        for element in res:
            flatten_element = flatten(
                element, reducer=make_reducer(DICT_FLATTEN_DELIMITER)
            )
            assert_that(flatten_element).does_not_contain_value(None)
