# Test imports
import pytest

# First party imports
from exodam_api import get_conn

# Local imports
from ...conftest import TESTS_SQL_FOLDER_PATH


@pytest.fixture(scope="function")
def remove_and_re_add_news_table():
    conn = get_conn()
    with open(TESTS_SQL_FOLDER_PATH / "drop_news_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    yield

    with open(TESTS_SQL_FOLDER_PATH / "create_news_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    conn.close()


@pytest.fixture(scope="function")
def remove_and_re_add_exodict_table():
    conn = get_conn()
    with open(TESTS_SQL_FOLDER_PATH / "drop_exodict_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    yield

    with open(TESTS_SQL_FOLDER_PATH / "create_exodict_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    conn.close()


@pytest.fixture(scope="function")
def remove_and_re_add_disk_table():
    conn = get_conn()
    with open(TESTS_SQL_FOLDER_PATH / "drop_disk_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    yield

    with open(TESTS_SQL_FOLDER_PATH / "create_disk_table.sql", "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()

    conn.close()
