"""Code for the module: Exodam_API."""

# Standard imports
from pathlib import Path
from tomllib import load as toml_load

# Local imports
from .clean_data import DICT_FLATTEN_DELIMITER, clean_none_data  # noqa: F401
from .connection import get_conn  # noqa: F401
from .convert_exodam import convert_exodam  # noqa: F401
from .file_checker import FileChecker  # noqa: F401
from .types import (  # noqa: F401
    ExodamResponseError,
    ExodamResponseVersion,
    ExodamResponseWithData,
    ExodamResponseWithoutData,
    SkelType,
)

_pyproject_path = Path(__file__).parent.parent / "pyproject.toml"

with open(_pyproject_path, "rb") as _pyproject_file:
    _pyproject_info = toml_load(_pyproject_file)["tool"]["poetry"]

__version__ = _pyproject_info["version"]
__app_name__ = _pyproject_info["name"]
__author__ = _pyproject_info["authors"]
__copyright__ = "LESIA"
__licence__ = "EUPL v1.2"
__contributors__ = ["Pierre-Yves MARTIN (LESIA)"]
