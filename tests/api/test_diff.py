# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Third party imports
from fastapi import status


@pytest.mark.parametrize(
    "files, exp_details, exp_data",
    [
        param(
            ["all_good.yaml", "all_good.yaml"],
            "No differences",
            {"files": ["all_good.yaml", "all_good.yaml"], "diff": True, "diff_str": ""},
            id="no diff - same file",
        ),
        param(
            ["all_good.yaml", "all_good_change_order.yml"],
            "No differences",
            {
                "files": ["all_good.yaml", "all_good_change_order.yml"],
                "diff": True,
                "diff_str": "",
            },
            id="no diff - change order",
        ),
        param(
            ["all_good.yaml", "all_good_little_different.yaml"],
            "Differences found",
            {
                "files": ["all_good.yaml", "all_good_little_different.yaml"],
                "diff": False,
                "diff_str": '35c35\n<             "uncert": 12.3,\n---\n>             '
                '"uncert": 12.5,\n',
            },
            id="diff found",
        ),
    ],
)
def test_api__diff_ok(testing_files_folder_path, client, files, exp_details, exp_data):
    diff_files_path = testing_files_folder_path / "diff"
    with open(f"{diff_files_path}/{files[0]}", "rb") as f1:
        with open(f"{diff_files_path}/{files[1]}", "rb") as f2:
            post_files = {"file_1": f1, "file_2": f2}

            response = client.post("/exodam/api/actions/diff/", files=post_files)

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)
        assert_that(json_response["details"]).is_equal_to(exp_details)
        assert_that(json_response["data"]).is_equal_to(exp_data)


@pytest.mark.parametrize(
    "files, exp_warnings",
    [
        # Wrong file extension
        param(
            ["all_good.yaml", "README.md"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. file 2 -> md",
        ),
        param(
            ["README.md", "all_good.yaml"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. file 1 -> md",
        ),
        param(
            ["README.md", "README.md"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Invalide type for file README.md. Expected `.yaml` or `.yml`, "
                "got: `.md`.",
            ],
            id="invalid ext. file 1, file 2 -> md",
        ),
        # Empty file
        param(
            ["all_good.yaml", "invalid.yaml"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file invalid.yaml is empty.",
            ],
            id="file 2 is empty",
        ),
        param(
            ["invalid.yaml", "all_good.yaml"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file invalid.yaml is empty.",
            ],
            id="file 1 is empty",
        ),
        param(
            ["invalid.yaml", "invalid.yaml"],
            [
                "Something went wrong when we try to read files.",
                "Exception TYPE: <class 'ValueError'>",
                "Empty File: The file invalid.yaml is empty.",
            ],
            id="file 1 and file 2 are empty",
        ),
    ],
)
def test_api__diff_unsupported_media_type(
    testing_files_folder_path, client, files, exp_warnings
):
    diff_files_path = testing_files_folder_path / "diff"
    with open(f"{diff_files_path}/{files[0]}", "rb") as f1:
        with open(f"{diff_files_path}/{files[1]}", "rb") as f2:
            post_files = {"file_1": f1, "file_2": f2}

            response = client.post("/exodam/api/actions/diff/", files=post_files)

    json_response = response.json()

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
        )
        assert_that(json_response["details"]).is_equal_to("Unsupported Media Type")
        assert_that(json_response["warnings"]).is_equal_to(exp_warnings)
