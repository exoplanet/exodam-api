# Remove __pycache__ and .pyc files and folders
clean:
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete

# Clear and display pwd
clear :
    clear
    pwd

# Install no-dev dependencies with poetry
install: clean clear
    poetry install --no-dev --remove-untracked

# install all dependencies with poetry and npm
install-all: clean clear
    poetry install --remove-untracked
    npm install
    sudo npm install markdownlint-cli2@0.4.0 --global

# Launch pytest without slow tests
test-no-slow : clear
    python3 -m pytest -m "not slow"

# Launch pytest only slow tests
test-slow : clear
    python3 -m pytest -m "slow"

# launch pytest
test-all : clear
    python3 -m pytest

# Launch test without slow tests
pytest : test-all clean

# Launch only slow test
pytest-slow: test-slow clean

# Launch all tests
pytest-fast: test-no-slow clean

# Update pre-commit hook and make a clean install of pre-commit dependencies
preupdate: clean clear
    pre-commit clean
    pre-commit autoupdate
    preinstall

# Do a clean install of pre-commit dependencies
preinstall: clean clear
    pre-commit install --hook-type pre-merge-commit
    pre-commit install --hook-type pre-push
    pre-commit install --hook-type post-rewrite
    pre-commit install-hooks
    pre-commit install

# Simulate a pre-commit check on added files
prepre: clean clear
    #!/usr/bin/env sh
    set -eux pipefail
    git status
    pre-commit run --all-files


# # Launch the mypy type linter on the module
mypy: clean clear
    mypy --pretty -p exodam_api --config-file pyproject.toml

# Run ruff
ruff path="main.py exodam_api": clean clear
    ruff {{path}}

# Run markdownlint
lintmd path='"**/*.md" "#node_modules" "#static"': clean clear
    markdownlint-cli2-config ".markdownlint-cli2.yaml" {{ path }}

# Run all linter
lint : clean clear onmy31 mypy ruff lintmd

# Run black and isort
onmy31 path ="main.py exodam_api tests": clean clear
    black {{path}}
    isort {{path}}

# auto interactive rebase
autorebase: clean clear
    git rebase -i $(git merge-base $(git branch --show-current) main)

# rebase on main
rebaseM: clean clear
    git checkout main
    git pull
    git checkout -
    git rebase main

# Launch coverage on all
coverage: clean clear
    coverage run -m pytest
    clear
    coverage report -m --skip-covered --precision=3

update-exodam-from-main:
    poetry shell
    poetry remove exodam
    poetry add git+ssh://git@gitlab.obspm.fr:exoplanet/exodam.git

update-exodam-from-branch branch:
    poetry shell
    poetry remove exodam
    poetry add git+ssh://git@gitlab.obspm.fr:exoplanet/exodam.git#{{branch}}

# Create exodam database for personals tests
create-exodam-db user="uchosson":
    psql -h localhost -U {{user}} -d postgres -c "CREATE DATABASE exodam"
    psql -h localhost -U {{user}} -d exodam -f tests/sql/schema.sql
    psql -h localhost -U {{user}} -d exodam -f tests/sql/data.sql

# Drop exodam database for personals tests
drop-exodam-db user:
    psql -h localhost -U {{user}} -d postgres -c "DROP DATABASE exodam"

# List all just commands
list :
    just --list
