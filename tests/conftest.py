# Test imports
import pytest
import pytest_postgresql  # noqa: F401
from pytest_postgresql.executor import PostgreSQLExecutor
from pytest_postgresql.janitor import DatabaseJanitor

# Standard imports
from os import environ as setenv
from os import getenv
from pathlib import Path
from typing import Generator

# Third party imports
from dotenv import load_dotenv
from dotmap import DotMap
from psycopg.conninfo import conninfo_to_dict, make_conninfo
from psycopg_pool import ConnectionPool

TESTS_FOLDER_PATH = Path(__file__).parent
TESTS_SQL_FOLDER_PATH = TESTS_FOLDER_PATH / "sql"
TESTING_FILE_FOLDER_PATH = TESTS_FOLDER_PATH / "testing_files"


@pytest.fixture(scope="session")
def tests_folder_path():
    return TESTS_FOLDER_PATH


@pytest.fixture(scope="session")
def testing_files_folder_path():
    return TESTING_FILE_FOLDER_PATH


@pytest.fixture(scope="session")
def db_password() -> str:
    """Return a stupid password for the temporary database created during the tests."""
    return "dummypassword"


def _load_schema(db: ConnectionPool) -> None:
    """
    Fill a database with a basic db schema.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(TESTS_SQL_FOLDER_PATH / "schema.sql", "rb") as sql_file:
        schema = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(schema)
                conn.commit()


def _load_data(db: ConnectionPool) -> None:
    """
    Fill a database with some data for tests.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(TESTS_SQL_FOLDER_PATH / "data.sql", "rb") as sql_file:
        data = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(data)
                conn.commit()


def _get_old_env_vars() -> dict[str, str]:
    load_dotenv()

    return {
        "old_user": getenv("LOCAL_DB_USER", default="toto"),
        "old_password": getenv("LOCAL_DB_PASSWORD", default="toto_strong_password"),
        "old_host": getenv("LOCAL_DB_HOST", default="toto_db_host"),
        "old_port": getenv("LOCAL_DB_PORT", default="toto_db_port"),
        "old_db_name": getenv("LOCAL_DB_DATABASE_NAME", default="toto_db_name"),
    }


def _set_env_vars_with_old(old_vars: dict[str, str]) -> None:
    setenv["LOCAL_DB_USER"] = old_vars["old_user"]
    setenv["LOCAL_DB_PASSWORD"] = old_vars["old_password"]
    setenv["LOCAL_DB_HOST"] = old_vars["old_host"]
    setenv["LOCAL_DB_PORT"] = old_vars["old_port"]
    setenv["LOCAL_DB_DATABASE_NAME"] = old_vars["old_db_name"]


def _set_environment_vars_for_exodam_core_db(user, password, host, port, dbname):
    setenv["LOCAL_DB_USER"] = user
    setenv["LOCAL_DB_PASSWORD"] = password
    setenv["LOCAL_DB_HOST"] = host
    setenv["LOCAL_DB_PORT"] = port
    setenv["LOCAL_DB_DATABASE_NAME"] = dbname


@pytest.fixture(scope="session", autouse=True)
def database_for_core(
    postgresql_proc: PostgreSQLExecutor,
    db_password: str,
) -> Generator[ConnectionPool, None, None]:
    """
    Return a temporary db with schema and data shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "exodam_test_core_db_with_data"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    conn_info = DotMap(conninfo_to_dict(conninfo))
    old_env_vars = _get_old_env_vars()
    _set_environment_vars_for_exodam_core_db(
        conn_info.user,
        conn_info.password,
        conn_info.host,
        conn_info.port,
        conn_info.dbname,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_schema(pool)
            _load_data(pool)
            yield

    _set_env_vars_with_old(old_env_vars)
