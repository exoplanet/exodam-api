"""FileChecker class."""

# Standard imports
from json import load as json_load
from pathlib import Path
from typing import Any, Self, cast

# Third party imports
from starlette.datastructures import UploadFile
from yaml import safe_load as yml_load

# Local imports
from .utils import have_good_suffix


class FileChecker:
    """FileChecker class used to check a file."""

    file: UploadFile | None
    "File to check."

    _file_path: Path
    "Path of the file to check."

    _file_suffix: str
    "Suffix of the file to check."

    _content: dict[str, Any] | None = None
    "Content of the file. Default: None."

    _err_msg: str | None = None
    "Error message if somethings wrong when we check the file. Default: None."

    _fail: bool = False
    "True if the validation failed, False otherwise. Default: False."

    def __init__(
        self,
        file_or_path: UploadFile | Path,
    ) -> None:
        """
        Initialize fileChecker class.

        Args:
            file_or_path: File or path of file to check.
        """
        match file_or_path:
            case UploadFile():
                self.file = file_or_path
                self._file_path = Path(cast(str, file_or_path.filename))
            case Path():
                self.file = None
                self._file_path = file_or_path
            case _:
                self._fail = True
                self._concat_err_msg(
                    "Invalide python type for the given args file_or_path. "
                    "Expected `starlette.datastructures.UploadFile` or `Path`, got: "
                    f"`{type(file_or_path)}`."
                )

        if not self._fail:
            self._file_suffix = self._file_path.suffix

    def _concat_err_msg(self, next_msg: str) -> None:
        """
        Concatenate a new error message with old error messages.

        If `self._err_msg` is None `next_msg` become `self._err_msg`
        else we concatenate `self._err_msg` and `next_msg`.

        Args:
            next_msg: New error message to concat is `self._err_msg`.
        """
        if self._err_msg is None:
            self._err_msg = next_msg
        else:
            self._err_msg = f"{self._err_msg} {next_msg}"

    def _load_content(self) -> None:
        """
        Load the content of `self.file`.

        If the fileChecker has not yet failed and the content was not yet loaded,
        we load it in `self.content`.
        else we do nothing.

        Raise:
            NotImplementedError: If the the type of the file is not supported.
        """
        if not self._fail and self._content is None:
            file_to_load = (
                open(self._file_path, "rb") if self.file is None else self.file.file
            )

            match self._file_suffix:
                case ".yaml" | ".yml":
                    self._content = yml_load(file_to_load)
                case ".json":
                    self._content = json_load(file_to_load)
                case _:
                    raise NotImplementedError()

            if self.file is None:
                file_to_load.close()

    def is_yaml(self) -> Self:
        """
        Check if `self.file` is a yaml file.

        Returns:
            Self.
        """
        if not self._fail:
            if not have_good_suffix(self._file_path):
                self._fail = True
                self._concat_err_msg(
                    f"Invalide type for file {self._file_path}. "
                    f"Expected `.yaml` or `.yml`, got: `{self._file_suffix}`."
                )
        return self

    def is_json(self) -> Self:
        """
        Check if `self.file` is a exodam json file.

        Returns:
            self.
        """
        if not self._fail:
            if not have_good_suffix(self._file_path):
                self._fail = True
                self._concat_err_msg(
                    f"Invalide type for file {self._file_path}. "
                    f"Expected `.exodam.json`, got: `{self._file_suffix}`."
                )
        return self

    def is_empty(self) -> Self:
        """
        Check if `self.content` is empty.

        Returns:
            self.
        """
        if not self._fail:
            self._load_content()
            if self._content is None:
                self._fail = True
                self._concat_err_msg(
                    f"Empty File: The file {self._file_path} is empty."
                )
        return self

    def none_or_raise(self) -> None:
        """
        Raise an error with `self._err_msg` if the fileChecker has failed.

        Otherwise, do nothing.

        Raises:
            ValueError: If the fileCheckere has failed before.
        """
        if self._fail:
            raise ValueError(self._err_msg)

    def content_or_raise(self) -> dict[str, Any] | None:
        """
        Load and returns the content of `self.file` if the fileChecker has not failed.

        If the fileCheck has failed raise an error with `self._err_msg`.

        Returns:
            Content of `self.file`

        Raise:
            ValueError: If the fileCheckere has failed before.
        """
        if self._fail:
            raise ValueError(self._err_msg)

        self._load_content()
        return self._content
