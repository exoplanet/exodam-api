# Test imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam_api import (
    ExodamResponseError,
    ExodamResponseVersion,
    ExodamResponseWithData,
    ExodamResponseWithoutData,
)


def test_exodam_response__with_data():
    resp = ExodamResponseWithData(details="toto", data={"a": 1, "b": 2})
    with soft_assertions():
        assert_that(resp.details).is_equal_to("toto")
        assert_that(resp.data).is_equal_to({"a": 1, "b": 2})


def test_exodam_response__without_data():
    resp = ExodamResponseWithoutData(details="toto")
    assert_that(resp.details).is_equal_to("toto")


def test_exodam_response__error():
    resp = ExodamResponseError(details="toto", warnings=["titi", "tutu"])
    with soft_assertions():
        assert_that(resp.details).is_equal_to("toto")
        assert_that(resp.warnings).is_equal_to(["titi", "tutu"])


def test_exodam_response__version():
    resp = ExodamResponseVersion(details="toto", version="0.12.0")
    with soft_assertions():
        assert_that(resp.details).is_equal_to("toto")
        assert_that(resp.version).is_equal_to("0.12.0")
