# Test imports
import pytest
from assertpy import assert_that, soft_assertions

# Exodam
from exodam import ExodictType

# Third party imports
from fastapi import status
from py_linq_sql import PyLinqSQLError


@pytest.mark.parametrize(
    "url, exodict_type",
    [
        ("/exodam/api/exodict/scientific", ExodictType.SCIENTIFIC),
        ("/exodam/api/exodict/editorial", ExodictType.EDITORIAL),
    ],
)
def test_get_scientific_exodict_api_point_works(client, mocker, url, exodict_type):
    # Let's fake the call to the underlying function since we are just testing the API
    expected = {"fake": "json"}
    fake_get_exodict = mocker.patch("main.get_exodict", return_value=expected)

    #  Call the API
    response = client.get(url)

    # Do we have a successful response?
    assert_that(response.status_code).is_equal_to(status.HTTP_200_OK)

    # Was the underlying function called?
    fake_get_exodict.assert_called_once_with(exodict_type)

    # Let's inspect the content
    assert_that(response.headers["content-type"]).is_equal_to("application/json")
    assert_that(response.json()).is_equal_to(expected)


@pytest.mark.parametrize(
    "exception",
    [
        ValueError,
        PyLinqSQLError,
    ],
)
@pytest.mark.parametrize(
    "url, exodict_type",
    [
        ("/exodam/api/exodict/scientific", ExodictType.SCIENTIFIC),
        ("/exodam/api/exodict/editorial", ExodictType.EDITORIAL),
    ],
)
def test_get_scientific_exodict_api_point_fails_get_exodict_fails(
    client, mocker, exception, url, exodict_type
):
    # Let's fake the call to the underlying function since we are just testing the API
    fake_get_exodict = mocker.patch("main.get_exodict", side_effect=exception)

    response = client.get(url)

    # Do we have the expected error code?
    assert_that(response.status_code).is_equal_to(status.HTTP_500_INTERNAL_SERVER_ERROR)

    # Was the underlying function called?
    fake_get_exodict.assert_called_once_with(exodict_type)

    # Let's inspect the content
    assert_that(response.headers["content-type"]).is_equal_to("application/json")
    json_content = response.json()
    with soft_assertions():
        assert_that(json_content["details"]).is_equal_to("Internal Server Error")
        assert_that(json_content["warnings"]).is_length(2)
        assert_that(json_content["warnings"][0]).starts_with(
            "Something went wrong when when retrieving exodict. "
            f"Exception TYPE: {exception}"
        )
